import React from 'react';
import {SafeAreaView, StatusBar, StatusBarStyle} from 'react-native';
import {withProviders} from '@shared/providers';
import '@shared/config/locales/i18n';
import {useTheme} from '@theme/hook/useTheme';
import {AuthStack} from './navigation/AuthStack';
import dayjs from 'dayjs';
import isToday from 'dayjs/plugin/isToday';
import isYesterday from 'dayjs/plugin/isYesterday';
import {MMKV} from '@shared/lib/initMMKV';
import {useMMKVStorage} from 'react-native-mmkv-storage';
import {Modal} from '@shared/ui/templates/Modal';
import {useStore} from 'effector-react';
import {MainStack} from './navigation/MainStack/index';
import {
  $currentSafeAreaColor,
  $isBottomBar,
} from '@shared/model/currentSafeAreaColor';

dayjs.extend(isToday);
dayjs.extend(isYesterday);

const App = () => {
  const {theme} = useTheme();
  const [accessToken] = useMMKVStorage('@accessToken', MMKV);
  const safeAreaBackgroundColor = useStore($currentSafeAreaColor);
  const isBottomBar = useStore($isBottomBar);
  console.log(1);
  console.log(accessToken); // чтобы легче было брать токен для тестов запросов
  return (
    <>
      <SafeAreaView
        edges={['top']}
        style={{flex: 0, backgroundColor: safeAreaBackgroundColor}}
      />
      <SafeAreaView
        edges={['left', 'right', 'bottom']}
        style={{
          flex: 1,
          backgroundColor: isBottomBar ? theme.white : safeAreaBackgroundColor,
          width: '100%',
        }}>
        <StatusBar
          backgroundColor={theme.background}
          barStyle={theme.darkBarStyle as StatusBarStyle}
        />
        {accessToken ? <MainStack /> : <AuthStack />}
        <Modal />
      </SafeAreaView>
    </>
  );
};

export default withProviders(App);
