import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {TabNavigation} from '../BottomTabNavigation';
import {MainStackParams} from '@shared/config/types/navigation/MainStackParams';

const Stack = createStackNavigator<MainStackParams>();

export function MainStack() {
  return (
    <Stack.Navigator
      screenOptions={({route}) => {
        return {
          headerShown: route.name !== 'TabNavigation',
          cardStyle: {
            backgroundColor: 'white',
          },
        };
      }}>
      <Stack.Screen key="BottomTabs" name="TabNavigation">
        {() => <TabNavigation />}
      </Stack.Screen>
      {/* <Stack.Screen options={{headerShown: false}} name="CashStack">
        {() => <CashStack />}
      </Stack.Screen>
      <Stack.Screen options={{headerShown: false}} name="CryptoStack">
        {() => <CryptoStack />}
      </Stack.Screen>
      <Stack.Screen options={{headerShown: false}} name="TransactionsStack">
        {() => <TransactionsStack />}
      </Stack.Screen> */}
    </Stack.Navigator>
  );
}
