import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {useTranslation} from 'react-i18next';
import {AuthStackParams} from '@shared/config/types/navigation/AuthStackParams';
import {Header} from '@shared/ui/organisms/Header';
import {WelcomeScreen} from '@pages/WelcomeScreen/ui';
import {LogIn} from '@pages/LoginScreen/ui';
import {TwoFactor} from '@pages/TwoFactor';

const Stack = createStackNavigator<AuthStackParams>();

export function AuthStack() {
  const {t} = useTranslation();
  return (
    <Stack.Navigator
      screenOptions={() => ({
        header: props => (
          <Header withBackButton headerTitle={props.options.title} />
        ),
      })}>
      <Stack.Screen
        options={{
          headerShown: false,
        }}
        name="WelcomeScreen"
        component={WelcomeScreen}
      />
      <Stack.Screen
        name="LogIn"
        component={LogIn}
        options={{
          title: t('login.login'),
        }}
      />
      <Stack.Screen
        name="TwoFactor"
        component={TwoFactor}
        options={{
          title: t('login.confirmLogIn'),
        }}
      />
      {/* <Stack.Screen
        name="TwoFactor"
        component={TwoFactor}
        options={{
          title: t('twoFactor.twoFactor'),
          header: props => (
            <Header
              withBackButton
              children={
                <View>
                  <Text fontWeight="700" fontSize={21}>
                    {props.options.title}
                  </Text>
                  <Text fontWeight="400" fontSize={12}>
                    {t('login.login')}
                  </Text>
                </View>
              }
              rightContent={<Icon name="guard" height={24} width={24} />}
            />
          ),
        }}
      />
      <Stack.Screen
        name="PasswordRecoveryEmail"
        component={PasswordRecoveryEmail}
        options={{title: t('passwordRecovery.recovery')}}
      />
      <Stack.Screen
        name="PasswordRecoveryCreate"
        component={PasswordRecoveryCreate}
        options={{title: t('passwordRecovery.recovery')}}
      />
      <Stack.Screen
        name="PasswordRecoverySuccess"
        component={PasswordRecoverySuccess}
        options={{
          title: t('passwordRecovery.recovery'),
          headerTitleAlign: 'center',
          headerLeft: () => null,
        }}
      /> */}
    </Stack.Navigator>
  );
}
