import React from 'react';
import {BottomTabs} from '@shared/ui/organisms/BottomTabs';
import {
  BottomTabBarProps,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';
import {useStore} from 'effector-react';
import {Header} from '@shared/ui/organisms/Header';
import {useTranslation} from 'react-i18next';
import {BottomTabsStackParams} from '@shared/config/types/navigation/BottomTabsStackParams';
import {NotificationButton} from '@shared/ui/molecules/NotificationButton';
import {HomeScreen} from '@pages/HomeScreen/ui';
import {BookingScreen} from '@pages/BookingScreen/ui';
import {BanquetScreen} from '@pages/BanquetScreen/ui';

const Tab = createBottomTabNavigator<BottomTabsStackParams>();

export const TabNavigation: React.FC = () => {
  const {t} = useTranslation();
  return (
    <Tab.Navigator
      key="BottomTabs"
      tabBar={({state}: BottomTabBarProps) => {
        return (
          <BottomTabs focused={state.index} routeNames={state.routeNames} />
        );
      }}
      screenOptions={() => ({
        header: props => (
          <Header withBackButton headerTitle={props.options.title} />
        ),
      })}>
      <Tab.Screen
        options={{
          header: () => (
            <Header
              titleFontSize={27}
              headerTitle="Hello!"
              rightContent={
                <>
                  <NotificationButton />
                </>
              }
            />
          ),
        }}
        name="HomeScreen"
        component={HomeScreen}
      />
      <Tab.Screen name="BookingScreen" component={BookingScreen} />
      <Tab.Screen name="BanquetScreen" component={BanquetScreen} />
    </Tab.Navigator>
  );
};
