import {
  ApolloClient,
  from,
  fromPromise,
  HttpLink,
  InMemoryCache,
} from '@apollo/client';
import {setContext} from '@apollo/client/link/context';
import {RetryLink} from '@apollo/client/link/retry';
import {onError} from '@apollo/client/link/error';
import {TokenDocument} from './__generated__';
import {sessionStore} from '../../model/sessionStore';
import {PUBLIC_URL} from '@app/env';
import {NativeModules} from 'react-native';
import {MMKV} from '@shared/lib/initMMKV';

const httpLink = new HttpLink({
  uri: `${PUBLIC_URL}/graphql`,
});

const retryLink = new RetryLink({
  delay: {
    initial: 300,
    max: Infinity,
    jitter: true,
  },
  attempts: {
    max: 5,
    retryIf: (error, _operation) => !!error,
  },
});

const authMiddleware = setContext(async () => {
  const token = MMKV.getString('@accessToken');

  return {
    headers: {
      authorization: token || '',
      timeout: 999999,
    },
  };
});

let isRefreshing = false;
let pendingRequests = [];

const resolvePendingRequests = () => {
  pendingRequests.map(callback => callback());
  pendingRequests = [];
};

const getNewToken = () => {
  const accessToken = MMKV.getString('@accessToken');
  const refreshToken = MMKV.getString('@refreshToken');
  MMKV.removeItem('@accessToken');
  return client.mutate({
    fetchPolicy: 'no-cache',
    mutation: TokenDocument,
    variables: {
      accessToken,
      refreshToken,
    },
  });
};

const errorLink = onError(({graphQLErrors, operation, forward}) => {
  if (graphQLErrors) {
    for (const err of graphQLErrors) {
      switch (err.message) {
        case 'Unauthorized':
        case 'Refresh token expired':
        case 'Refresh token not found': {
          MMKV.clearStore();
          NativeModules.DevSettings.reload();
          break;
        }
        case 'Authorization token is not valid': {
          let forward$;
          if (!isRefreshing) {
            isRefreshing = true;
            forward$ = fromPromise(
              getNewToken()
                .then(response => {
                  MMKV.setString(
                    '@accessToken',
                    response.data.token.accessToken,
                  );
                  MMKV.setString(
                    '@refreshToken',
                    response.data.token.refreshToken,
                  );
                  sessionStore.setSession();
                  resolvePendingRequests();
                })
                .catch(() => {
                  pendingRequests = [];
                  return;
                })
                .finally(() => {
                  isRefreshing = false;
                }),
            )
              .filter(Boolean)
              .flatMap(accessToken => {
                const oldHeaders = operation.getContext().headers;
                operation.setContext({
                  headers: {
                    ...oldHeaders,
                    authorization: accessToken,
                  },
                });

                return forward(operation);
              });
          } else {
            forward$ = fromPromise(
              new Promise(resolve => {
                pendingRequests.push(() => resolve(true));
              }),
            );
          }

          return forward$.flatMap(() => forward(operation));
        }
      }
    }
  }
});

export const client = new ApolloClient({
  cache: new InMemoryCache({
    typePolicies: {
      TradeFuture: {merge: true},
      TradeSpot: {merge: true},
    },
    addTypename: false,
  }),
  link: from([errorLink, retryLink, authMiddleware, httpLink]),
  defaultOptions: {
    query: {
      fetchPolicy: 'no-cache',
    },
    mutate: {
      fetchPolicy: 'network-only',
    },
  },
});
