export function isEmptyObject<T>(obj: T): boolean {
  if (Object.keys(obj).length) {
    return false;
  }
  return true;
}
