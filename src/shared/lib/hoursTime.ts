import dayjs from 'dayjs';

export const hoursTime = (time: Date | string) => {
  return dayjs(time).format('HH:mm:ss');
};
