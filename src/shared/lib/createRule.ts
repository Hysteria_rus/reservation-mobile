import * as yup from 'yup';
import {Rule} from 'effector-forms';

export function createRule<TValue, T = any>({
  schema,
  name,
}: {
  schema: yup.SchemaOf<T>;
  name: string;
}): Rule<TValue> {
  return {
    name,
    validator: (value: TValue) => {
      try {
        schema.validateSync(value);
        return {
          isValid: true,
          value,
        };
      } catch (err) {
        return {
          isValid: false,
          value,
          errorText: err.message,
        };
      }
    },
  };
}
