import {
  createNavigationContainerRef,
  StackActions,
} from '@react-navigation/native';
import {StackParams} from '@shared/config/types/navigation/StackParams';

export const navigationRef = createNavigationContainerRef();

export function navigate(name: never, params: never) {
  if (navigationRef.isReady()) {
    navigationRef.navigate(name, params);
  }
}

export function goBack() {
  if (navigationRef.isReady()) {
    const popAction = StackActions.pop(1);
    navigationRef.dispatch(popAction);
  }
}

export function getNavigationStackKey() {
  if (navigationRef.isReady()) {
    return navigationRef.current?.getState()?.key;
  }
  return null;
}

export function getNavigationScreenParams(): Readonly<
  StackParams | undefined
> | null {
  if (navigationRef.isReady()) {
    return navigationRef.getCurrentRoute()?.params;
  }
  return null;
}
