import {useTheme} from '@theme/hook/useTheme';
import {CashStatus, CryptoStatus} from '@shared/api/apollo/__generated__';

export const useStatusColor = (status: CryptoStatus | CashStatus) => {
  const {theme} = useTheme();

  switch (true) {
    case status === 'COMPLETED':
      return theme.success;
    case status === 'PENDING':
      return theme.grey;
    case status === 'DENIED':
      return theme.rejected;
  }
};
