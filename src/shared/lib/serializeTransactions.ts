import dayjs from 'dayjs';
import {
  CashTransaction,
  CryptoTransaction,
} from '@shared/api/apollo/__generated__';

type SerializeTransactions = CashTransaction[] | CryptoTransaction[];

export const serializeTransactions = (transactions: SerializeTransactions) => {
  let group: Record<string, any> = {};
  transactions.forEach(item => {
    const date = dayjs(item.time).format('YYYY-MM-DD').toString();
    if (group[date]) {
      group[date].push(item);
    } else {
      group[date] = [item];
    }
  });
  return group;
};
