import React from 'react';
import {createEvent, createStore, sample} from 'effector';
import {ModalKey} from '@shared/config/modalEnum';

type ActiveModalType = {
  component: React.FC;
  key: string;
};

const openModal = createEvent<ActiveModalType>();
const closeModal = createEvent<string>('');
const closeAllModals = createEvent<void>();

const $listModals = createStore<ActiveModalType[]>([])
  .on(openModal, (state, payload) => {
    return [payload, ...state];
  })
  .reset(closeAllModals);

const $openedModal = createStore<ActiveModalType>({
  component: $listModals[0],
  key: ModalKey.DefaultModal,
}).reset($listModals.reset());

sample({
  clock: closeModal,
  source: $listModals,
  fn: (source, clock) => {
    if (clock.length) {
      return source.filter(item => item.key !== clock);
    }
    return source.slice(1, source.length - 1);
  },
  target: $listModals,
});

sample({
  clock: $listModals,
  fn: clock => clock[0],
  target: $openedModal,
});

export {closeAllModals, openModal, $listModals, $openedModal};
