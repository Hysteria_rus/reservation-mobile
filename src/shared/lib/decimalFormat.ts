function formatParts(parts: string[], scale: number) {
  const partInteger = parts[0];
  const partDecimal = `${parts[1].slice(0, scale)}`;
  return `${partInteger},${partDecimal}`;
}

const DOT_SYMBOL = '.';
const COMMA_SYMBOL = ',';

export const decimalsFormat = (
  val: string,
  scale: number,
  maxValue?: number,
) => {
  if (val) {
    const value = val.replace(/[^\d,.]/g, '');
    if (maxValue) {
      if (Number(value) > maxValue) {
        return;
      }
    }
    if (!value.includes(COMMA_SYMBOL) && value.indexOf(DOT_SYMBOL) >= 0) {
      return formatParts(value.split(DOT_SYMBOL), scale);
    }

    if (value.indexOf(COMMA_SYMBOL) >= 0) {
      return formatParts(value.split(COMMA_SYMBOL), scale);
    }
    return value;
  }
  return val;
};

/**
 *  Trims raw string value to required decimal scale without rounding
 *   @param value: value to trim
 *   @param scale: decimal scale to trim to, if none provided then trims to integer
 *   @returns {string} returns trimmed string value
 *
 */
export const trimDecimals = (value: string, scale: number) => {
  if (value.length) {
    if (value.includes('.')) {
      const parts = value.split('.');
      if (!scale) {
        return parts[0];
      }
      return parts[0] + '.' + parts[1]?.slice(0, scale);
    } else {
      return value;
    }
  }

  return '';
};
