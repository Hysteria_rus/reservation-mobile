import dayjs from 'dayjs';
import {useTranslation} from 'react-i18next';

export const useFormattedDate = () => {
  const {t} = useTranslation();

  const getFormattedDate = (date: Date | string) => {
    const dayjsDate = dayjs(date);
    const formattedDate = dayjsDate.format('DD.MM');

    if (dayjsDate.isToday()) {
      return `${t('timePeriod.today')}, ${formattedDate}`;
    }

    if (dayjsDate.isYesterday()) {
      return `${t('timePeriod.yesterday')}, ${formattedDate}`;
    }
    return dayjsDate.format('dddd, DD.MM');
  };

  return {getFormattedDate};
};
