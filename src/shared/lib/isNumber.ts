const numberRegexp = /([^0-9(.|,)]+)/;

export const isNumber = (value: string) => !numberRegexp.test(value);
