import {GetRatesQuery} from '@shared/api/apollo/__generated__';
import BigNumber from 'bignumber.js';

export type CountRateType = {
  rates: GetRatesQuery['getRates'];
  currentCurrencyCode: string;
  targetCurrencyCode: string;
  amount: string;
  decimalScale?: number;
};

export const calculateRate = (input: CountRateType) => {
  if (
    input.targetCurrencyCode?.toUpperCase() ===
    input.currentCurrencyCode?.toUpperCase()
  ) {
    return new BigNumber(input.amount).decimalPlaces(8).toString() || '0';
  }

  const countRate = (currentCurrency: string, purposeCurrency: string) => {
    return new BigNumber(
      input.rates.find(
        rate =>
          rate.baseAsset.ticker === currentCurrency.toUpperCase() &&
          rate.quoteAsset.ticker === purposeCurrency.toUpperCase() &&
          rate.priceInBase,
      )?.priceInBase || 0,
    );
  };

  if (isNaN(countRate('BTC', input.currentCurrencyCode).toNumber())) {
    return '0';
  }

  return (
    countRate('BTC', input.targetCurrencyCode)
      .dividedBy(countRate('BTC', input.currentCurrencyCode).toNumber() || 1)
      .multipliedBy(input.amount || 0)
      .decimalPlaces(input.decimalScale || 8)
      .toString() || '0'
  );
};
