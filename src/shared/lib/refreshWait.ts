export const refreshWait = (timeout: number) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
};
