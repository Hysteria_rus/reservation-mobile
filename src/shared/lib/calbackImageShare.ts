import RNShare from 'react-native-share';

export function callbackImageShare(dataURL: string) {
  let shareImageBase64 = {
    url: `data:image/png;base64,${dataURL}`,
  };
  RNShare.open(shareImageBase64).catch((error: string) => console.log(error));
}
