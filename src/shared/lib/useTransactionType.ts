import {useTranslation} from 'react-i18next';
import {
  CardTransactionTypeEnum,
  CashCategory,
  CryptoCategory,
} from '@shared/api/apollo/__generated__';
import {useCallback} from 'react';
import {TransactionsCategory} from '@shared/config/enums/transactions';
import {FormattedTransactionType} from '@shared/config/types/FormattedTransactionType';

export const useTransactionType = (
  transactionCategory: TransactionsCategory,
) => {
  const {t} = useTranslation();

  const shapeTransactionType = (
    type: FormattedTransactionType['category'],
  ): FormattedTransactionType[] => {
    return Object.values(type).map(item => ({
      category: item,
      text: t(`transactions.${item}`) as string,
      icon: item.toLowerCase(),
    }));
  };

  const transactionType = useCallback(() => {
    switch (true) {
      case transactionCategory === TransactionsCategory.CASH:
        return shapeTransactionType(CashCategory);
      case transactionCategory === TransactionsCategory.CRYPTO:
        return shapeTransactionType(CryptoCategory);
      case transactionCategory === TransactionsCategory.CARDS:
        return shapeTransactionType(CardTransactionTypeEnum);
    }
  }, [transactionCategory]);

  return transactionType();
};
