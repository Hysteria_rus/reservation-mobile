export const regexPassword = new RegExp(
  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&+=`!*[{\]}]).{6,64}$/m,
);
export const numberRegex = /^\d*\.?\d*$/;
export const addressRegex = /^(?=[a-zA-Z0-9])[a-zA-Z1-9./\-', ]*$/;
export const regexEmail = new RegExp(
  /^((?=[a-zA-Z0-9])[a-zA-Z0-9-_]{1,25})@(([a-zA-Z0-9-]){1,25}\.)([a-zA-Z0-9]{2,4})$/m,
);
