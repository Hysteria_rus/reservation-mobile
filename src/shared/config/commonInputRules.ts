import * as yup from 'yup';
import {createRule} from '@shared/lib/createRule';
import {regexEmail, regexPassword} from '@shared/constants/regexes';

export const commonInputRules = {
  required: createRule<string>({
    name: 'required',
    schema: yup.string().required('errors.required'),
  }),
  password: createRule({
    name: 'password',
    schema: yup.string().matches(regexPassword, 'errors.password'),
  }),
  repeatPassword: {
    name: 'repeatPassword',
    validator: (repeatPassword: string, {password}: {password: string}) =>
      repeatPassword === password,
    errorText: 'errors.repeatPassword',
  },
  email: createRule({
    name: 'email',
    schema: yup.string().matches(regexEmail, 'errors.incorrectEmail'),
  }),
};
