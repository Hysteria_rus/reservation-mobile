import {
  CardTransactionTypeEnum,
  CashCategory,
  CryptoCategory,
} from '@shared/api/apollo/__generated__';
import {IconName} from '../iconsList';

export type FormattedTransactionType = {
  category:
    | typeof CashCategory
    | typeof CryptoCategory
    | typeof CardTransactionTypeEnum;
  text: string;
  icon: IconName;
};
