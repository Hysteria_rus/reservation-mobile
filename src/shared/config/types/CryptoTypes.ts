export type CryptoAccount = {
  id: number;
  currencyName: string;
  ticker: string;
  cryptoBalance: string;
  fiatBalance: string;
  valueChange?: string;
};
