export type TfaValuesType = {
  EMAIL?: string;
  SMS?: string;
  OTP?: string;
};
