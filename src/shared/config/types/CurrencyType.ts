export enum CryptoCurrencyEnum {
  bnb = 'bnb',
  btc = 'btc',
  eth = 'eth',
  usdt = 'usdt',
  eos = 'eos',
  universal = 'universal',
  ocean = 'ocean',
  comp = 'comp',
}

//TODO: потом тип сгенерится с бэка
export enum CurrencyTypeEnum {
  Crypto = 'CRYPTO',
  Fiat = 'FIAT',
}
export enum RiskLevelEnum {
  High = 'HIGH',
  Low = 'LOW',
  Medium = 'MEDIUM',
}

export enum PositionStatusEnum {
  Investing = 'INVESTING',
  None = 'NONE',
  Withdrawing = 'WITHDRAWING',
}

//

export interface CurrencyType {
  name: string;
  symbol?: string;
  id?: string;
  code: string;
  type?: CurrencyTypeEnum;
  precision?: number;
}

export interface CryptoCurrencyType extends CurrencyType {
  blockchain?: string;
  code: string;
}
