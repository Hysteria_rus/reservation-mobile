import {
  CashCategory,
  CashStatus,
  CashTransaction,
  CryptoStatus,
  CryptoTransaction,
} from '@shared/api/apollo/__generated__';

export type Transaction = CashTransaction | CryptoTransaction;

export type FormattedTransactions = {
  title: Date | string;
  data: FormattedTransactionsData[];
};

export type FormattedTransactionsData = {
  amount: string;
  category: CashCategory;
  address: string;
  time: Date | string;
  currency: string;
  status: CashStatus | CryptoStatus;
  currencySymbol: string;
  country?: string;
  iban?: string;
  bic?: string;
  accountNumber?: string;
  swift?: string;
  companyName?: string;
  isRepeatable: boolean;
  hash?: string;
};
