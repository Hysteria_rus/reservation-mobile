import {ReactElement} from 'react';
import {StyleProp, TextInputProps, ViewStyle} from 'react-native';

export type RenderInput = {
  label?: string;
  placeholder?: string;
  onChangeText: (text: string) => void;
  value: string;
  errorText?: string;
  style?: StyleProp<ViewStyle>;
  inputProps?: Omit<TextInputProps, 'value' | 'onChangeText' | 'placeholder'>;
  rightContent?: ReactElement;
  upContent?: ReactElement;
};
