export type MainStackParams = {
  HomeScreen: undefined;
  CashScreen: undefined;
  CashStack: undefined;
  CryptoScreen: undefined;
  CryptoStack: undefined;
  HubScreen: undefined;
  ActionScreen: undefined;
  TransactionsStack: undefined;
  TabNavigation: undefined;
};
