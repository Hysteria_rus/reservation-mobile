import {TfaType} from '@shared/api/apollo/__generated__';
import {TfaValuesType} from '../TfaTypes';

export type StackParams = {
  confirm?: (values: TfaValuesType, operations?: Array<TfaType>) => void;
};
