export type CryptoStackParams = {
  CryptoAccount: {
    name: string;
  };
  EmptyReceiveCrypto: undefined;
  CryptoReceive: {
    name: string;
  };
  TwoFactor: undefined;
  SelectCrypto: undefined;
  SendCrypto: undefined;
  CompleteSend: undefined;
  SendCryptoChoseScreen: undefined;
};
