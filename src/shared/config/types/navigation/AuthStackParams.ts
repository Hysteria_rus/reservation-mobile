import {StackParams} from './StackParams';

export type AuthStackParams = {
  WelcomeScreen: undefined;
  LogIn: undefined;
  TwoFactor: StackParams;
  PasswordRecoveryEmail: undefined;
  PasswordRecoveryCode: undefined;
  PasswordRecoveryCreate: undefined;
  PasswordRecoverySuccess: undefined;
};
