import {
  CryptoCurrencyType,
  CurrencyType,
  PositionStatusEnum,
  RiskLevelEnum,
} from '@shared/config/types/CurrencyType';
import {AccountNetwork} from '@shared/config/types/NetworkTypes';

type Document = {
  id: string;
  name: string;
};

export enum AccountWalletType {
  crypto = 'crypto',
  fiat = 'fiat',
  earn = 'earn',
}

enum EarnAccountWalletType {
  crypto = 'crypto',
  fiat = 'fiat',
}

interface Balance {
  availableAmount?: string;
}

interface CryptoBalance extends Balance {
  totalAmount?: string;
  heldAmount?: string;
  totalBalanceToBtc?: string;
}

interface EarnBalance extends Balance {
  estimatedAmountPrice: string;
  totalInvest?: string;
  change: string;
  availableAmountPrice: string;
  dailyChangePrice: string;
  dailyChangePercent: string;
  dailyChange: string;
  changeCurrency: string;
}

interface AccountType {
  id: string;
  accountName: string;
  balance: Balance;
  currency: CurrencyType;
  type?: AccountWalletType;
  isOutgoing?: boolean;
}

interface FiatAccountType extends AccountType {
  documents: Document[];
  iban: string;
  walletId: string;
}

interface CryptoAccountType extends AccountType {
  currency: CryptoCurrencyType;
  balance: CryptoBalance;
  defaultNetwork: AccountNetwork;
  pnl?: string | null;
  address?: string;
}

interface EarnAccountType extends AccountType {
  accountWalletType: EarnAccountWalletType;
  balance: EarnBalance;
  isPayOff: boolean;
  sourceAccountId: string;
  payOffStart: number;
  payOffEnd: number;
  risk?: RiskLevelEnum;
  status?: PositionStatusEnum;
}

export type {FiatAccountType, CryptoAccountType, AccountType, EarnAccountType};
