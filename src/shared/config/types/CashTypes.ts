import {FiatAccountType} from '@shared/config/types/AccountTypes';

export type CashLastTransactionType = {
  id: number;
  type: string;
  from: string;
  value: string;
  status: string;
  time: string;
};

export type CashAccount = Omit<FiatAccountType, 'documents'>;
