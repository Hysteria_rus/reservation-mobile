import {IconName} from '@shared/config/iconsList';

export interface PrimaryCurrencyType {
  id: number;
  value: string;
  label: string;
  cryptoAnalog: string;
  icon: IconName;
  height?: number;
  width?: number;
}
