import welcome from '../../shared/svg/Welcome.svg';
import backArrow from '../../shared/svg/BackArrow.svg';
import notifications from '../../shared/svg/Notifications.svg';

export const iconsList = {
  welcome,
  backArrow,
  notifications,
};

export type IconName = keyof typeof iconsList;
