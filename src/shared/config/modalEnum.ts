export enum ModalKey {
  DefaultModal = 'DefaultModal',
  ActionScreen = 'ActionScreen',
  BottomSheetNetworks = 'BottomSheetNetworks',
  CryptoAccountsModal = 'CryptoAccountsModal',
  SelectCurrency = 'SelectCurrency',
  TopUpModal = 'TopUpModal',
  FilterModal = 'FilterModal',
}
