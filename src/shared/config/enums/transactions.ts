export enum TransactionsCategory {
  CRYPTO = 'crypto',
  CASH = 'cash',
  CARDS = 'cards',
}

export enum CryptoTransactionsType {
  BUY = 'buy',
  SELL = 'sell',
  SEND = 'send',
  RECEIVE = 'receive',
}

export enum CashTransactionsType {
  PAYMENT = 'payment',
  WITHDRAW = 'withdraw',
  DEPOSIT = 'deposit',
  TRANSFER = 'transfer',
}

export enum CardTransactionsType {
  PAYMENT = 'payment',
  WITHDRAW = 'withdraw',
  DEPOSIT = 'deposit',
}
