export enum TimePeriod {
  DAY = '24H',
  WEEK = '7D',
  MONTH = '1M',
  QUARTER = '3M',
  HALF_YEAR = '6M',
  YEAR = '1Y',
}
