import {StyleSheet, ViewStyle} from 'react-native';

const styles = StyleSheet.create({
  viewInput: {
    marginTop: 8,
    borderRadius: 8,
    width: '100%',
    height: 56,
    borderWidth: 1,
    paddingHorizontal: 16,
    alignItems: 'center',
    flexDirection: 'row',
  },
  input: {
    maxWidth: '100%',
    flex: 1,
    fontSize: 14,
    lineHeight: 18,
  },
  content: {
    marginLeft: 10,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    viewInput: ({
      borderColor,
      backgroundColor,
    }: Pick<ViewStyle, 'borderColor' | 'backgroundColor'>) => ({
      ...styles.viewInput,
      borderColor,
      backgroundColor,
    }),
  };
};
