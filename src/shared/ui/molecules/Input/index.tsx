import React, {ReactElement, useState} from 'react';
import {
  KeyboardTypeOptions,
  Pressable,
  StyleProp,
  TextInput,
  TextInputProps,
  TextStyle,
  View,
  ViewStyle,
} from 'react-native';
import {useStyles} from '@shared/ui/molecules/Input/styles';
import {useTheme} from '@theme/hook/useTheme';
import {Icon} from '@shared/ui/atoms/Icon';
import {Tooltip} from '@shared/ui/atoms/Tooltip';
import {Text} from '@shared/ui/atoms/Text';
import {isNumber} from '@shared/lib/isNumber';
import {decimalsFormat} from '@shared/lib/decimalFormat';

interface InputProps {
  label?: string;
  inputProps?: TextInputProps;
  rightContent?: ReactElement;
  upContent?: ReactElement;
  errorText?: string;
  style?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  wrapperStyle?: StyleProp<ViewStyle>;
  keyboardType?: KeyboardTypeOptions;
  isDisabled?: boolean;
  isNumeric?: boolean;
  decimalScale?: number;
  regexFilter?: string;
  onChange: (text: string) => void;
  value: string;
  maxValue?: number;
}

export const Input: React.FC<InputProps> = ({
  label,
  inputProps,
  rightContent,
  errorText,
  style,
  upContent,
  textStyle,
  wrapperStyle,
  isDisabled,
  isNumeric,
  decimalScale,
  onChange,
  regexFilter,
  value,
  maxValue,
}) => {
  const {theme} = useTheme();
  const styles = useStyles();
  const [errorIsShowing, setErrorIsShowing] = useState<boolean>(false);
  const [prevVal, setPrevVal] = useState('');
  const currentRegexFilter = regexFilter ? new RegExp(regexFilter) : null;

  const onInputChange = (newValue: string) => {
    if (isNumeric && !isNumber(newValue)) {
      return;
    }
    if (decimalScale && newValue.split(',')[1]?.length > decimalScale) {
      return prevVal;
    } else if (decimalScale === 0) {
      if (
        Number.isNaN(Number(newValue.slice(-1))) ||
        Number.isNaN(Number(newValue))
      ) {
        return prevVal;
      }
    }

    if (regexFilter) {
      if (!currentRegexFilter?.test(newValue) && newValue) {
        return prevVal;
      }
    }
    setPrevVal(newValue);

    if (isNumeric) {
      onChange(newValue.replace(/,/g, '.'));
    } else {
      onChange(newValue);
    }
  };
  return (
    <View style={style}>
      {upContent}
      <Text fontSize={16} lineHeight={24} fontWeight="600">
        {label}
      </Text>
      <View
        style={[
          styles.viewInput({
            borderColor: errorText ? theme.rejected : theme.primary,
            backgroundColor: isDisabled ? theme.background : theme.white,
          }),
          wrapperStyle,
        ]}>
        <TextInput
          keyboardType={isNumeric ? 'decimal-pad' : 'default'}
          style={[styles.input, textStyle]}
          value={
            decimalScale ? decimalsFormat(value, decimalScale, maxValue) : value
          }
          onChangeText={onInputChange}
          placeholderTextColor={theme.background}
          editable={!isDisabled}
          {...inputProps}
        />
        {errorText && (
          <Tooltip
            tooltipProps={{
              placement: 'bottom',
            }}
            isVisible={errorIsShowing}
            onClose={() => setErrorIsShowing(false)}
            content={<Text>{errorText}</Text>}>
            <Pressable onPress={() => setErrorIsShowing(true)}>
              <Icon name="info" color={theme.rejected} />
            </Pressable>
          </Tooltip>
        )}
        {rightContent && <View style={styles.content}>{rightContent}</View>}
      </View>
    </View>
  );
};
