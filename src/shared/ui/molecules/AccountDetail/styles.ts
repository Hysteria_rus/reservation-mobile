import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    borderRadius: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 16,
    paddingHorizontal: 12,
  },
});

export const useStyles = () => ({
  ...styles,
  container: (backgroundColor: string) => ({
    ...styles.container,
    backgroundColor,
  }),
});
