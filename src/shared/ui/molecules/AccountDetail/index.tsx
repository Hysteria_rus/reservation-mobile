import React, {ReactElement} from 'react';
import {StyleProp, View, ViewStyle} from 'react-native';
import {useStyles} from '@shared/ui/molecules/AccountDetail/styles';
import {useTheme} from '@theme/hook/useTheme';
import {Text} from '@shared/ui/atoms/Text';

interface AccountDetailProps {
  title: string;
  children: ReactElement;
  style?: StyleProp<ViewStyle>;
}

export const AccountDetail: React.FC<AccountDetailProps> = ({
  title,
  children,
  style,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();
  return (
    <View style={[styles.container(theme.infoBoardBackground), style]}>
      <Text>{title}</Text>
      <View>{children}</View>
    </View>
  );
};
