import React, {ReactElement} from 'react';
import {View, ViewStyle} from 'react-native';
import {useStyles} from '@shared/ui/molecules/AccountItem/styles';
import {Icon} from '@shared/ui/atoms/Icon';
import {IconName} from '@shared/config/iconsList';

interface AccountItemProps {
  accountIconName: IconName;
  leftContent: ReactElement;
  rightContent?: ReactElement;
  leftWrapperStyle?: ViewStyle;
  rightWrapperStyle?: ViewStyle;
  wrapperStyle?: ViewStyle;
}

export const AccountItem: React.FC<AccountItemProps> = ({
  accountIconName,
  leftContent,
  rightContent,
  leftWrapperStyle,
  rightWrapperStyle,
  wrapperStyle,
}) => {
  const styles = useStyles();

  return (
    <View style={[styles.wrapper, wrapperStyle]}>
      <View style={styles.leftContentWrapper}>
        <Icon name={accountIconName} width={32} height={32} />
        <View style={[styles.leftWrapper, leftWrapperStyle]}>
          {leftContent}
        </View>
      </View>
      {rightContent && (
        <View style={[styles.rightWrapper, rightWrapperStyle]}>
          {rightContent}
        </View>
      )}
    </View>
  );
};
