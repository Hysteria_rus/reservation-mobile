import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    height: 40,
    justifyContent: 'space-between',
  },
  leftContentWrapper: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'row',
    flex: 1,
  },
  leftWrapper: {
    height: '100%',
    flex: 1,
    marginLeft: 8,
    justifyContent: 'space-between',
  },
  rightWrapper: {
    alignItems: 'flex-end',
    marginLeft: 8,
  },
});
export const useStyles = () => {
  return {
    ...styles,
  };
};
