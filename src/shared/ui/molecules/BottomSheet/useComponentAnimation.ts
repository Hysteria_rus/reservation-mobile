import {
  useAnimatedStyle,
  useSharedValue,
  withSpring,
} from 'react-native-reanimated';
import {useCallback} from 'react';
import {Gesture} from 'react-native-gesture-handler';
import {SCREEN_HEIGHT} from '@shared/constants/screenSize';

type BottomSheetAnimation = {
  onClose: () => void;
  height: number;
};

const maxTranslateY = -SCREEN_HEIGHT + 50;

export const useBottomSheetAnimation = ({
  onClose,
  height,
}: BottomSheetAnimation) => {
  const translateY = useSharedValue(0);

  const context = useSharedValue({y: 0});

  const scrollTo = useCallback((value: number) => {
    'worklet';
    translateY.value = withSpring(value, {damping: 50});
  }, []);

  const gesture = Gesture.Pan()
    .onStart(() => {
      context.value = {y: translateY.value};
    })
    .onUpdate(event => {
      translateY.value = event.translationY + context.value.y;
      translateY.value = Math.max(translateY.value, maxTranslateY);
    })
    .onEnd(() => {
      if (translateY.value > -height) {
        scrollTo(0);
        onClose();
      }
      if (translateY.value < -SCREEN_HEIGHT / 2) {
        scrollTo(maxTranslateY);
      }
    });

  const animatedStyles = useAnimatedStyle(() => {
    return {
      transform: [{translateY: translateY.value}],
    };
  });

  return {gesture, animatedStyles, scrollTo};
};
