import {StyleSheet} from 'react-native';
import {SCREEN_HEIGHT, SCREEN_WIDTH} from '@shared/constants/screenSize';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  component: {
    borderTopLeftRadius: 28,
    borderTopRightRadius: 28,
    width: SCREEN_WIDTH,
    padding: 16,

    position: 'absolute',

    overflow: 'hidden',
  },

  line: {
    width: 32,
    height: 4,
    alignSelf: 'center',
    marginTop: 8,
    borderRadius: 8,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    component: (backgroundColor: string, height: number) => ({
      ...styles.component,
      backgroundColor,
      top: SCREEN_HEIGHT,
      height: height,
    }),
    line: (backgroundColor: string) => ({
      ...styles.line,
      backgroundColor,
    }),
  };
};
