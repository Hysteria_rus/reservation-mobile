import React, {ReactElement, useCallback, useEffect} from 'react';
import {
  GestureDetector,
  GestureHandlerRootView,
} from 'react-native-gesture-handler';
import {View} from 'react-native';
import Animated, {runOnJS} from 'react-native-reanimated';
import {useBottomSheetAnimation} from '@shared/ui/molecules/BottomSheet/useComponentAnimation';
import {useStyles} from '@shared/ui/molecules/BottomSheet/styles';
import {useTheme} from '@theme/hook/useTheme';
import {SCREEN_HEIGHT} from '@shared/constants/screenSize';
import {closeAllModals} from '@shared/lib/modalFactory';

interface BottomSheetProps {
  children: ReactElement | ReactElement[];
  height?: number;
}

export const BOTTOM_SHEET_DEFAULT_HEIGHT = SCREEN_HEIGHT / 3;

export const BottomSheet: React.FC<BottomSheetProps> = ({
  height = BOTTOM_SHEET_DEFAULT_HEIGHT,
  children,
}) => {
  const {theme} = useTheme();
  const styles = useStyles();

  const handleOnClose = useCallback(() => {
    'worklet';
    runOnJS(closeAllModals)();
  }, []);

  const {gesture, animatedStyles, scrollTo} = useBottomSheetAnimation({
    onClose: handleOnClose,
    height,
  });

  useEffect(() => {
    scrollTo(-height);
  }, []);

  return (
    <GestureHandlerRootView style={styles.container}>
      <GestureDetector gesture={gesture}>
        <Animated.View
          style={[styles.component(theme.white, height), animatedStyles]}>
          <View style={styles.line(theme.inputBorderColor)} />
          {children}
        </Animated.View>
      </GestureDetector>
    </GestureHandlerRootView>
  );
};
