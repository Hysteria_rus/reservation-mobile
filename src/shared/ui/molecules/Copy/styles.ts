import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  text: {
    maxWidth: 150,
    textAlign: 'right',
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export const useStyles = () => {
  return {
    ...styles,
    icon: (marginLeft: number) => ({
      marginLeft,
    }),
  };
};
