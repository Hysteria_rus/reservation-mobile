import React from 'react';
import Clipboard from '@react-native-clipboard/clipboard';
import {Pressable} from 'react-native';
import {Icon} from '@shared/ui/atoms/Icon';
import {useStyles} from '@shared/ui/molecules/Copy/styles';
import {Text, TextProps} from '@shared/ui/atoms/Text';
import {IconName} from '@shared/config/iconsList';

interface CopyBoardProps {
  text: string;
  textProps?: Omit<TextProps, 'children'>;
  icon?: boolean;
  value?: string;
  iconName?: IconName;
  iconColor?: string;
  iconMarginLeft?: number;
}

export const Copy: React.FC<CopyBoardProps> = ({
  text,
  textProps,
  icon = true,
  value,
  iconName = 'copy',
  iconColor,
  iconMarginLeft = 6,
}) => {
  const onCopyClick = () => {
    Clipboard.setString(value || text);
  };
  const styles = useStyles();
  return (
    <Pressable onPress={onCopyClick} style={styles.row}>
      <Text
        config={{
          numberOfLines: 1,
          ellipsizeMode: 'middle',
        }}
        style={styles.text}
        {...textProps}>
        {text}
      </Text>
      {icon && (
        <Icon
          name={iconName}
          width={16}
          height={16}
          color={iconColor}
          style={styles.icon(iconMarginLeft)}
        />
      )}
    </Pressable>
  );
};
