import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  button: {
    width: 32,
    height: 32,
    borderWidth: 0,
    borderRadius: 8,
    marginRight: 4,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    button: (backgroundColor: string) => ({
      ...styles.button,
      backgroundColor,
    }),
  };
};
