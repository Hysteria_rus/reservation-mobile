import React from 'react';
import {useTheme} from '@theme/hook/useTheme';
import {useStyles} from './styles';
import {DefaultButton} from '@shared/ui/atoms/DefaultButton';
import {Icon} from '@shared/ui/atoms/Icon';

export const NotificationButton: React.FC = () => {
  const {theme} = useTheme();
  const styles = useStyles();

  return (
    <DefaultButton
      buttonType="secondary"
      viewStyle={styles.button(theme.white)}
      onPress={() => {}}
      children={
        <Icon
          name="notifications"
          width={16}
          height={16}
          color={theme.primary}
        />
      }
    />
  );
};
