import React from 'react';
import {View, StyleProp, ViewStyle} from 'react-native';
import {useStyles} from '@shared/ui/molecules/CopyBoard/styles';
import {useTheme} from '@theme/hook/useTheme';
import {Copy} from '@shared/ui/molecules/Copy';
import {IconName} from '@shared/config/iconsList';
import {Text} from '@shared/ui/atoms/Text';

interface CopyBoardProps {
  title: string;
  info: string;
  iconName?: IconName;
  iconColor?: string;
  viewStyle?: StyleProp<ViewStyle>;
}

export const CopyBoard: React.FC<CopyBoardProps> = ({
  title,
  info,
  iconName,
  iconColor,
  viewStyle,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();

  return (
    <View style={[styles.wrapper(theme.infoBoardBackground), viewStyle]}>
      <Text fontSize={14} lineHeight={18} fontWeight="400">
        {title}
      </Text>
      <Copy text={info} iconName={iconName} iconColor={iconColor} />
    </View>
  );
};
