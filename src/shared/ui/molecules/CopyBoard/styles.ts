import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    width: '100%',
    height: 50,
    borderRadius: 12,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    paddingLeft: 12,
    paddingRight: 15,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    wrapper: (backgroundColor: string) => ({
      ...styles.wrapper,
      backgroundColor,
    }),
  };
};
