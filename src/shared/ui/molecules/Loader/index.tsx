import React, {memo, useLayoutEffect} from 'react';
import {View, ViewStyle} from 'react-native';
import {styles} from './styles';
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  withRepeat,
  withTiming,
  Easing,
} from 'react-native-reanimated';
import {Icon} from '@shared/ui/atoms/Icon';

export interface LoaderProps {
  containerStyle?: ViewStyle;
  width?: number;
  height?: number;
  color?: string;
}

export const Loader: React.FC<LoaderProps> = memo(
  ({containerStyle, width = 40, height = 40, color = 'white'}) => {
    const corner = useSharedValue(0);

    useLayoutEffect(() => {
      corner.value = withRepeat(
        withTiming(360, {
          duration: 2000,
          easing: Easing.linear,
        }),
        -1,
      );
    }, [corner]);

    const style = useAnimatedStyle(() => {
      return {
        transform: [
          {
            rotate: corner.value + 'deg',
          },
        ],
        width: width,
        height: height,
      };
    });

    return (
      <View style={containerStyle}>
        <View style={styles.loaderContainer}>
          <Animated.View style={style}>
            <Icon name="loader" height={width} width={height} color={color} />
          </Animated.View>
        </View>
      </View>
    );
  },
);
