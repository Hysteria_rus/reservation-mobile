import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  loaderContainer: {
    width: 77,
    height: 75,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
  },
  absolute: {
    position: 'absolute',
  },
});
