import React from 'react';
import {View} from 'react-native';
import {Tab, TabProps} from '@shared/ui/atoms/Tab';
import {useStyles} from '@shared/ui/molecules/Tabs/styles';
import {useTheme} from '@theme/hook/useTheme';

interface Tabs {
  tabs: TabProps[];
}

export const Tabs: React.FC<Tabs> = ({tabs}) => {
  const {theme} = useTheme();
  const styles = useStyles();

  return (
    <View style={styles.container(theme.infoBoardBackground)}>
      {tabs.map((tab, index) => (
        <Tab
          key={tab.label + index}
          label={tab.label}
          isActive={tab.isActive}
          onPress={tab.onPress}
        />
      ))}
    </View>
  );
};
