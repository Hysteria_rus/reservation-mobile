import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    borderRadius: 12,
    paddingVertical: 4,
    paddingHorizontal: 8,
  },
});

export const useStyles = () => ({
  container: (backgroundColor: string) => ({
    ...styles.container,
    backgroundColor,
  }),
});
