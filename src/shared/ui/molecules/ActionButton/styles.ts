import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    height: 128,
    width: 128,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 32,
  },
  text: {
    marginTop: 8,
    textAlign: 'center',
  },
  icon: {
    height: 16,
    width: 16,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    container: (backgroundColor: string) => ({
      ...styles.container,
      backgroundColor,
    }),
    text: (color: string) => ({
      ...styles.text,
      color,
    }),
    icon: (color: string) => ({
      ...styles.icon,
      color,
    }),
  };
};
