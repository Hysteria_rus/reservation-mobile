import React from 'react';
import {Pressable, StyleProp, ViewStyle} from 'react-native';
import {SCREEN_HEIGHT} from '@shared/constants/screenSize';
import {IconName} from '@shared/config/iconsList';
import {useStyles} from './styles';
import {useTheme} from '@theme/hook/useTheme';
import {Icon} from '@shared/ui/atoms/Icon';
import {Text} from '@shared/ui/atoms/Text';

interface ActionButtonProps {
  onPress: () => void;
  icon: IconName;
  text: string;
  style?: StyleProp<ViewStyle>;
}

export const BOTTOM_SHEET_DEFAULT_HEIGHT = SCREEN_HEIGHT / 3;

export const ActionButton: React.FC<ActionButtonProps> = ({
  onPress,
  icon,
  text,
  style,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();
  return (
    <Pressable
      onPress={onPress}
      style={[styles.container(theme.primary), style]}>
      <Icon
        style={styles.icon(theme.white)}
        name={icon}
        width={16}
        height={16}
      />
      <Text fontSize={12} lineHeight={14} style={styles.text(theme.white)}>
        {text}
      </Text>
    </Pressable>
  );
};
