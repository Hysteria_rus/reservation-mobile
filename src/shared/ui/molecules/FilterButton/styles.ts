import {StyleSheet, ViewStyle} from 'react-native';

export const styles = StyleSheet.create({
  selectButton: {
    borderWidth: 1,
    paddingVertical: 5,
    marginTop: 24,
  },
  wrapper: {
    maxWidth: 'auto',
    flexDirection: 'row',
  },
});

export const useStyles = () => ({
  ...styles,
  selectButton: ({
    borderColor,
    backgroundColor,
  }: Pick<ViewStyle, 'borderColor' | 'backgroundColor'>) => ({
    ...styles.selectButton,
    borderColor,
    backgroundColor,
  }),
});
