import React from 'react';
import {SelectButton} from '@shared/ui/atoms/SelectButton';
import {Text} from '@shared/ui/atoms/Text';
import {useTheme} from '@theme/hook/useTheme';
import {useStyles} from '@shared/ui/molecules/FilterButton/styles';
import {View} from 'react-native';

interface FilterButtonProps {
  text: string;
  selectFilter: () => void;
  clearFilter: () => void;
  isSelected: boolean;
}

export const FilterButton: React.FC<FilterButtonProps> = ({
  text,
  selectFilter,
  clearFilter,
  isSelected,
}) => {
  const {theme} = useTheme();
  const styles = useStyles();

  return (
    <View style={styles.wrapper}>
      <SelectButton
        onPress={isSelected ? clearFilter : selectFilter}
        style={styles.selectButton({
          backgroundColor: isSelected
            ? theme.primary
            : theme.infoBoardBackground,
          borderColor: isSelected ? theme.primary : theme.inputBorderColor,
        })}
        icon={isSelected ? 'closeSmall' : 'dropDownArrow'}
        arrowColor={isSelected ? theme.white : theme.primary}>
        <Text
          fontSize={12}
          fontWeight="400"
          color={isSelected ? theme.white : theme.black}>
          {text}
        </Text>
      </SelectButton>
    </View>
  );
};
