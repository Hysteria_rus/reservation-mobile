import React from 'react';
import QR from 'react-native-qrcode-svg';
import {Pressable, View} from 'react-native';
import {styles} from '@shared/ui/molecules/QR/styles';
import {useTheme} from '@theme/hook/useTheme';
import {useTranslation} from 'react-i18next';
import {Text} from '@shared/ui/atoms/Text';

interface QRCodeProps {
  qrValue: string;
  getRef: any;
  onSharePress: () => void;
}

export const QRCode: React.FC<QRCodeProps> = ({
  qrValue,
  getRef,
  onSharePress,
}) => {
  const {theme} = useTheme();
  const {t} = useTranslation();

  return (
    <View style={styles.container}>
      <QR size={180} quietZone={10} value={qrValue} getRef={getRef} />
      <Pressable onPress={onSharePress}>
        <Text color={theme.primary} fontWeight="400">
          {t('cryptoReceive.copyQr')}
        </Text>
      </Pressable>
    </View>
  );
};
