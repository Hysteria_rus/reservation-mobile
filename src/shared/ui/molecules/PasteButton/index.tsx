import React from 'react';
import {Pressable, Text} from 'react-native';
import {useStyles} from './styles';
import {useTheme} from '@theme/hook/useTheme';
import {useTranslation} from 'react-i18next';
import Clipboard from '@react-native-clipboard/clipboard';

interface PasteButtonProps {
  setValue: (value: string) => void;
}

export const PasteButton: React.FC<PasteButtonProps> = ({setValue}) => {
  const {t} = useTranslation();
  const styles = useStyles();

  const {theme} = useTheme();

  const pasteText = async () => {
    const text = await Clipboard.getString();
    setValue(text);
  };
  return (
    <Pressable onPress={pasteText}>
      <Text style={styles.buttonText(theme.primary)}>{t('common.paste')}</Text>
    </Pressable>
  );
};
