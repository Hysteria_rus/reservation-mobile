import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 14,
    fontWeight: '600',
  },
});

export const useStyles = () => ({
  ...styles,
  buttonText: (color: string) => ({
    ...styles.buttonText,
    color,
  }),
});
