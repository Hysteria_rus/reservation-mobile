import React from 'react';
import {BottomSheet} from '@shared/ui/molecules/BottomSheet';
import {useStore} from 'effector-react';
import {transactionCategoryModal} from '@shared/ui/widgets/TransactionType';
import {Text} from '@shared/ui/atoms/Text';
import {Pressable} from 'react-native';
import {Icon} from '@shared/ui/atoms/Icon';
import {IconName} from '@shared/config/iconsList';
import {styles} from '@shared/ui/widgets/TransactionType/styles';
import {useTheme} from '@theme/hook/useTheme';
import {FormattedTransactionType} from '@pages/Transactions/lib/transactionType';
import {WINDOW_HEIGHT} from '@shared/constants/screenSize';

interface TransactionCategoryProps {
  onPressByCategory: (category: FormattedTransactionType) => void;
  transactionTypes: FormattedTransactionType[];
}

export const TransactionType: React.FC<TransactionCategoryProps> = ({
  onPressByCategory,
  transactionTypes,
}) => {
  const isVisible = useStore(transactionCategoryModal.$store);
  const handleOnClose = () => transactionCategoryModal.setVisible(false);
  const {theme} = useTheme();
  return (
    <BottomSheet
      height={WINDOW_HEIGHT / 2}
      isVisible={isVisible}
      onClose={handleOnClose}>
      {transactionTypes.map(item => (
        <Pressable onPress={() => onPressByCategory(item)} style={styles.row}>
          <Icon
            width={16}
            height={18}
            color={theme.primary}
            name={item.icon as IconName}
          />
          <Text style={styles.text} fontWeight="400">
            {item.text}
          </Text>
        </Pressable>
      ))}
    </BottomSheet>
  );
};
