import {StyleSheet, ViewStyle} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    height: 80,
  },
  title: {
    fontWeight: '700',
  },
  content: {
    alignItems: 'center',
  },
  rightContent: {
    marginLeft: 'auto',
    flexDirection: 'row',
  },
  icon: {
    marginRight: 16,
  },
});

export const useStyles = () => ({
  ...styles,
  container: (backgroundColor: string) => ({
    ...styles.container,
    backgroundColor,
  }),
  content: ({justifyContent}: Pick<ViewStyle, 'justifyContent'>) => ({
    ...styles.content,
    justifyContent,
  }),
});
