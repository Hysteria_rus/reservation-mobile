import React from 'react';
import {useStyles} from './styles';
import {View, ViewStyle} from 'react-native';
import {BackButton} from '@shared/ui/atoms/BackButton';
import {useTheme} from '@theme/hook/useTheme';
import {useStore} from 'effector-react';
import {$currentSafeAreaColor} from '@shared/model/currentSafeAreaColor';
import {Text} from '@shared/ui/atoms/Text';

interface HeaderProps {
  withBackButton?: boolean;
  headerTitle?: string;
  children?: React.ReactNode;
  rightContent?: React.ReactNode;
  titleFontSize?: number;
}

export const Header: React.FC<HeaderProps> = ({
  withBackButton,
  headerTitle,
  children,
  rightContent,
  titleFontSize = 21,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();
  const backgroundColor = useStore($currentSafeAreaColor);

  const isJustifyCenter = (): Pick<ViewStyle, 'justifyContent'> => {
    if (rightContent || withBackButton) {
      return 'flex-start' as Pick<ViewStyle, 'justifyContent'>;
    }
    return 'center' as Pick<ViewStyle, 'justifyContent'>;
  };

  return (
    <View style={styles.container(backgroundColor || theme.white)}>
      {withBackButton && (
        <View style={styles.icon}>
          <BackButton />
        </View>
      )}
      <View style={styles.content(isJustifyCenter())}>
        {headerTitle ? (
          <Text style={styles.title} fontSize={titleFontSize}>
            {headerTitle}
          </Text>
        ) : (
          children
        )}
      </View>
      <View style={styles.rightContent}>{rightContent}</View>
    </View>
  );
};
