import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 12,
    marginTop: 12,
    justifyContent: 'space-between',
    borderRadius: 8,
    flexWrap: 'wrap',
  },
});

export const useStyles = () => ({
  container: (backgroundColor: string) => ({
    ...styles.container,
    backgroundColor,
  }),
});
