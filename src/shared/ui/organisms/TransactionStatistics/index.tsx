import React from 'react';
import {View} from 'react-native';
import {TransactionStatItem} from '@shared/ui/atoms/TransactionStatItem';
import {useStyles} from '@shared/ui/organisms/TransactionStatistics/styles';
import {useTheme} from '@theme/hook/useTheme';
import {useTranslation} from 'react-i18next';

interface TransactionStatisticProps {
  statistic: any;
}

export const TransactionStatistic: React.FC<TransactionStatisticProps> = ({
  statistic,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();
  const {t} = useTranslation();

  if (!statistic) {
    return null;
  }

  return (
    <View style={styles.container(theme.infoBoardBackground)}>
      {statistic.map((item, index) => (
        <TransactionStatItem
          key={item.type + index}
          iconName={item.iconName}
          type={t(`transactions.${item.type}`)}
          amount={item.amount}
        />
      ))}
    </View>
  );
};
