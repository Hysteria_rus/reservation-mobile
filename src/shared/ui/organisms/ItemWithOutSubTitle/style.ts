import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  contentView: {
    justifyContent: 'center',
  },
  leftContent: {
    fontSize: 14,
    lineHeight: 18,
  },
  rightContent: {
    fontSize: 14,
    lineHeight: 18,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    leftContent: (color: string) => ({
      ...styles.leftContent,
      color,
    }),
    rightContent: (color: string) => ({
      ...styles.rightContent,
      color,
    }),
  };
};
