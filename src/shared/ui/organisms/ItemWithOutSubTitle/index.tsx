import React from 'react';
import {AccountItem} from '@shared/ui/molecules/AccountItem';
import {IconName} from '@shared/config/iconsList';
import {useStyles} from '@shared/ui/organisms/ItemWithOutSubTitle/style';
import {Text} from '@shared/ui/atoms/Text';

interface ItemWithOutSubTitleProps {
  accountIconName: IconName;
  leftContent?: string;
  rightContent?: string;
}

export const ItemWithOutSubTitle: React.FC<ItemWithOutSubTitleProps> = ({
  accountIconName,
  leftContent,
  rightContent,
}) => {
  const styles = useStyles();
  return (
    <AccountItem
      accountIconName={accountIconName}
      rightWrapperStyle={styles.contentView}
      leftWrapperStyle={styles.contentView}
      leftContent={
        <Text fontSize={14} lineHeight={18} fontWeight="400">
          {leftContent}
        </Text>
      }
      rightContent={
        <Text fontSize={14} lineHeight={18} fontWeight="700">
          {rightContent}
        </Text>
      }
    />
  );
};
