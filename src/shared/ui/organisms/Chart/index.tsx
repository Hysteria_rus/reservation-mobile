import React from 'react';
import {LineChart} from 'react-native-chart-kit';
import {WINDOW_WIDTH} from '@shared/constants/screenSize';
import {LineChartData} from 'react-native-chart-kit/dist/line-chart/LineChart';
import {useChartConfig} from '@shared/ui/organisms/Chart/config';
import {StyleProp, View, ViewStyle} from 'react-native';
import {useStyles} from '@shared/ui/organisms/Chart/styles';
import {useTheme} from '@theme/hook/useTheme';
import {ChartTab, ChartTabProps} from '@shared/ui/atoms/ChartTab';
import {ChartRange} from '@shared/ui/atoms/ChartRange';

interface ChartProps {
  data: LineChartData;
  tabs: ChartTabProps[];
  range: {
    upper: string;
    lover: string;
  };
  style?: StyleProp<ViewStyle>;
}

export const Chart: React.FC<ChartProps> = ({data, tabs, range, style}) => {
  const {theme} = useTheme();
  const chartConfig = useChartConfig();
  const styles = useStyles();

  return (
    <View style={[styles.wrapper(theme.white), style]}>
      <ChartRange value={range.upper} />
      <LineChart
        data={data}
        width={WINDOW_WIDTH}
        height={180}
        chartConfig={chartConfig}
        fromZero
        withInnerLines={false}
        withVerticalLines={false}
        withHorizontalLines={false}
        withVerticalLabels={false}
        withHorizontalLabels={false}
        style={styles.chart}
      />
      <ChartRange value={range.lover} />
      <View style={styles.tabs}>
        {tabs.map((tab, index) => (
          <ChartTab
            key={index + tab.value}
            label={tab.label}
            isActive={tab.isActive}
            value={tab.value}
            onPress={tab.onPress}
          />
        ))}
      </View>
    </View>
  );
};
