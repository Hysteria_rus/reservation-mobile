import {useTheme} from '@theme/hook/useTheme';
import {ChartConfig} from 'react-native-chart-kit/dist/HelperTypes';

export const useChartConfig = () => {
  const {theme} = useTheme();
  const config: ChartConfig = {
    backgroundGradientFromOpacity: 0,
    backgroundGradientToOpacity: 0,
    decimalPlaces: 2, // optional, defaults to 2dp
    fillShadowGradientTo: theme.white,
    color: () => theme.primary,
    labelColor: () => theme.black,
    style: {
      borderRadius: 16,
    },

    strokeWidth: 2,
    propsForDots: {
      r: '1',
      strokeWidth: '0.01',
      stroke: theme.inputBorderColor,
    },
  };

  return config;
};
