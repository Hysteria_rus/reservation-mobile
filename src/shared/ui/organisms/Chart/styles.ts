import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  chart: {
    paddingRight: 0,
    marginLeft: -16,
    marginBottom: -20,
    marginTop: -10,
  },
  tabs: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 16,
  },
});

export const useStyles = () => ({
  ...styles,
  wrapper: (backgroundColor: string) => ({
    backgroundColor,
  }),
});
