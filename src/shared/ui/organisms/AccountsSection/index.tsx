import React, {ReactElement} from 'react';
import {View} from 'react-native';
import {useTheme} from '@theme/hook/useTheme';
import {useTranslation} from 'react-i18next';
import {numberFormat} from '@shared/lib/numberFormat';
import {Skeleton} from '@shared/ui/atoms/Skeleton';
import {Text} from '@shared/ui/atoms/Text';
import {useStyles} from './styles';

interface AccountsSectionProps {
  title: string;
  totalBalance: string;
  tabs: ReactElement[];
  accounts: ReactElement[];
  bottomContent?: ReactElement;
  currencyTicker?: string;
  isLoadingAccounts?: boolean;
}

export const AccountsSection: React.FC<AccountsSectionProps> = ({
  title,
  totalBalance,
  tabs,
  accounts,
  bottomContent,
  currencyTicker = '',
  isLoadingAccounts = false,
}) => {
  const {theme} = useTheme();
  const styles = useStyles();
  const {t} = useTranslation();
  return (
    <View>
      <Text fontWeight="700" fontSize={21} lineHeight={24}>
        {title}
      </Text>
      <Skeleton isLoading={!Number(totalBalance)} width={150} height={15}>
        <View style={styles.totalBalanceWrapper}>
          <Text
            fontWeight="400"
            fontSize={12}
            lineHeight={14}
            color={theme.grey}>
            {`${t('home.totalBalance')}:`}
          </Text>
          <Text
            fontWeight="400"
            fontSize={12}
            lineHeight={14}
            style={styles.totalBalance}>
            {numberFormat({
              number: totalBalance,
              prefix: currencyTicker,
              decimal: 2,
            })}
          </Text>
        </View>
      </Skeleton>
      <View style={styles.tabWrapper}>{tabs}</View>

      <Skeleton
        style={styles.skeleton}
        isLoading={isLoadingAccounts}
        width="100%"
        height={30}>
        <>
          <View>{accounts}</View>
          <View>{bottomContent}</View>
        </>
      </Skeleton>
    </View>
  );
};
