import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  title: {
    marginBottom: 3,
  },
  totalBalance: {
    fontWeight: '400',
    fontSize: 12,
    lineHeight: 14,
    marginLeft: 4,
  },
  tabWrapper: {
    flexDirection: 'row',
    marginTop: 10,
  },
  skeleton: {
    marginBottom: 12,
  },
  totalBalanceWrapper: {
    marginTop: 3,
    flexDirection: 'row',
  },
});

export const useStyles = () => {
  return {
    ...styles,
  };
};
