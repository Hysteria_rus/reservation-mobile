import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  component: {
    paddingVertical: 30,
  },
  title: {marginBottom: 20},
  item: {
    marginBottom: 24,
  },
  time: {
    marginLeft: 40,
  },
});
