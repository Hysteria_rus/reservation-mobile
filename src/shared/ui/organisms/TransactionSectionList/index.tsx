import React from 'react';
import {SectionList, Pressable} from 'react-native';
import {Text} from '@shared/ui/atoms/Text';
import {ItemWithSubTitle} from '@shared/ui/organisms/ItemWithSubTitle';
import {numberFormat} from '@shared/lib/numberFormat';
import {useFormattedDate} from '@shared/lib/formattedDate';
import {
  FormattedTransactions,
  FormattedTransactionsData,
} from '@shared/config/types/transaction';
import {useTheme} from '@theme/hook/useTheme';
import {styles} from '@shared/ui/organisms/TransactionSectionList/styles';
import {useTranslation} from 'react-i18next';
import {hoursTime} from '@shared/lib/hoursTime';

interface TransactionSectionListProps {
  transactions: FormattedTransactions[];
  onLoadMode?: () => void;
  onPressByItem: (item: FormattedTransactionsData) => void;
}

export const TransactionSectionList: React.FC<TransactionSectionListProps> = ({
  transactions,
  onLoadMode,
  onPressByItem,
}) => {
  const {theme} = useTheme();
  const {getFormattedDate} = useFormattedDate();
  const {t} = useTranslation();

  return (
    <SectionList
      nestedScrollEnabled
      showsVerticalScrollIndicator={false}
      contentContainerStyle={styles.component}
      sections={transactions}
      onEndReached={onLoadMode}
      keyExtractor={(item, index) => String(item.time) + index}
      renderSectionHeader={({section: {title}}) => (
        <Text style={styles.title} fontSize={18} fontWeight="700">
          {getFormattedDate(title)}
        </Text>
      )}
      renderItem={({item}) => (
        <Pressable onPress={() => onPressByItem(item)} style={styles.item}>
          <ItemWithSubTitle
            accountIconName="outgoing"
            rightTitle={numberFormat({
              number: item.amount,
              decimal: 2,
              suffix: item.currency,
            })}
            rightSubTitle={item.status}
            leftTitle={t(`transactions.${item.category}`)}
            leftSubTitle={item.address}
          />
          <Text style={styles.time} fontSize={12} color={theme.grey}>
            {hoursTime(item.time)}
          </Text>
        </Pressable>
      )}
    />
  );
};
