import React from 'react';
import {useTheme} from '@theme/hook/useTheme';
import {styles} from '@shared/ui/organisms/ItemWithSubTitle/styles';
import {IconName} from '@shared/config/iconsList';
import {AccountItem} from '@shared/ui/molecules/AccountItem';
import {Text} from '@shared/ui/atoms/Text';

interface ItemWithSubTitleProps {
  accountIconName: IconName;
  leftTitle: string;
  leftSubTitle?: string;
  rightTitle?: string;
  rightSubTitle?: string;
}

export const ItemWithSubTitle: React.FC<ItemWithSubTitleProps> = ({
  accountIconName,
  leftTitle,
  leftSubTitle,
  rightTitle,
  rightSubTitle,
}) => {
  const {theme} = useTheme();
  return (
    <AccountItem
      accountIconName={accountIconName}
      leftWrapperStyle={styles.contentWrapper}
      rightWrapperStyle={styles.contentWrapper}
      leftContent={
        <>
          <Text fontWeight="400" fontSize={14} lineHeight={18}>
            {leftTitle}
          </Text>
          <Text
            config={{
              numberOfLines: 1,
              ellipsizeMode: 'middle',
            }}
            fontWeight="400"
            fontSize={12}
            lineHeight={14}
            color={theme.grey}>
            {leftSubTitle}
          </Text>
        </>
      }
      rightContent={
        <>
          <Text fontWeight="600" fontSize={14} lineHeight={18}>
            {rightTitle}
          </Text>
          <Text
            fontWeight="400"
            fontSize={12}
            lineHeight={14}
            color={theme.grey}>
            {rightSubTitle}
          </Text>
        </>
      }
    />
  );
};
