export enum BottomTabEnum {
  HomeScreen = 'Home',
  CashScreen = 'Cash',
  CryptoScreen = 'Crypto',
  HubScreen = 'Hub',
  ActionScreen = 'Arrows',
}
