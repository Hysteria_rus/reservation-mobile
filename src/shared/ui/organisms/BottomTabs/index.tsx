import {useNavigation, useFocusEffect} from '@react-navigation/native';
import {IconName} from '@shared/config/iconsList';
import {setIsBottomBar} from '@shared/model/currentSafeAreaColor';
import {useTheme} from '@theme/hook/useTheme';
import React, {useCallback} from 'react';
import {View, Pressable, Text} from 'react-native';
import {Icon} from '../../atoms/Icon';
import {BottomTabEnum} from './config';
import {useStyles} from './styles';

interface BottomTabItemProps {
  routeNames: string[];
  focused: number;
}

export const BottomTabs: React.FC<BottomTabItemProps> = ({
  focused,
  routeNames,
}) => {
  const navigation = useNavigation();
  const {theme} = useTheme();

  useFocusEffect(
    useCallback(() => {
      setIsBottomBar(true);
      return () => {
        setIsBottomBar(false);
      };
    }, []),
  );

  const styles = useStyles();

  const getTabName = (route: string) => {
    return BottomTabEnum[route as keyof typeof BottomTabEnum].toLowerCase();
  };

  const getColor = (index: number) => {
    return focused === index ? theme.primary : theme.inputBorderColor;
  };

  return (
    <View style={styles.container}>
      {routeNames.map((item, index) => (
        <View key={index}>
          <Pressable
            style={styles.tab('white', 0)}
            onPress={() => navigation.navigate(item)}>
            <Icon
              style={styles.icon(getColor(index))}
              name={getTabName(item).toLowerCase() as IconName}
            />
            <Text style={styles.text(getColor(index))}>{getTabName(item)}</Text>
          </Pressable>
        </View>
      ))}
    </View>
  );
};
