import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 32,
    height: 68,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    shadowColor: 'black',
    shadowOpacity: 0.26,
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    elevation: 10,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  icon: {
    display: 'flex',
  },
  tab: {
    height: 44,
    width: 44,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 12,
    fontWeight: '500',
    lineHeight: 12,
    marginTop: 8,
    textTransform: 'capitalize',
  },
});

export const useStyles = () => {
  return {
    ...styles,
    icon: (color: string) => ({
      ...styles.icon,
      color,
    }),
    text: (color: string) => ({
      ...styles.text,
      color,
    }),
    tab: (backgroundColor: string, borderRadius: number) => ({
      ...styles.tab,
      backgroundColor,
      borderRadius,
    }),
  };
};
