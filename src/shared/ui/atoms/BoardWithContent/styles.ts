import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export const useStyles = () => {
  return {
    ...styles,
    wrapper: (
      width?: number,
      height?: number,
      backgroundColor?: string,
      borderRadius?: number,
    ) => ({
      ...styles.wrapper,
      width,
      height,
      backgroundColor,
      borderRadius,
    }),
  };
};
