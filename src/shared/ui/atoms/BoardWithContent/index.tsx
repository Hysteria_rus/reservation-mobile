import {StyleProp, View, ViewStyle} from 'react-native';
import React, {ReactElement} from 'react';
import {useStyles} from '@shared/ui/atoms/BoardWithContent/styles';

interface BoardWithContentProps {
  width?: number;
  height?: number;
  backgroundColor?: string;
  borderRadius?: number;
  children: ReactElement;
  wrapperStyle?: StyleProp<ViewStyle>;
}

export const BoardWithContent: React.FC<BoardWithContentProps> = ({
  width,
  height,
  backgroundColor,
  borderRadius,
  children,
  wrapperStyle,
}) => {
  const styles = useStyles();
  return (
    <View
      style={[
        styles.wrapper(width, height, backgroundColor, borderRadius),
        wrapperStyle,
      ]}>
      {children}
    </View>
  );
};
