import React from 'react';
import {View} from 'react-native';
import {numberFormat} from '@shared/lib/numberFormat';
import BigNumber from 'bignumber.js';
import {useTranslation} from 'react-i18next';
import {replaceComma} from '@shared/lib/replaceComma';
import {styles} from '@shared/ui/atoms/CryptoBalance/styles';
import {Text} from '@shared/ui/atoms/Text';

interface CryptoBalanceProps {
  balance: string;
  rate: string;
}

export const CryptoBalance: React.FC<CryptoBalanceProps> = ({
  balance,
  rate,
}) => {
  const {t} = useTranslation();

  return (
    <View>
      <Text style={styles.title}>{t('common.yourBalance')}</Text>
      <Text style={styles.balance}>
        <Text style={styles.highlighted}>
          {numberFormat({number: replaceComma(balance)})}
        </Text>
        {numberFormat({
          number: new BigNumber(replaceComma(balance)).multipliedBy(
            replaceComma(rate),
          ),
          prefix: '~',
        })}
      </Text>
    </View>
  );
};
