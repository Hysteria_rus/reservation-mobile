import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  title: {
    fontSize: 12,
  },
  balance: {
    fontSize: 16,
    marginTop: 4,
  },
  highlighted: {
    fontWeight: '700',
  },
});
