import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  range: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  line: {
    borderWidth: 1,
    borderStyle: 'dashed',
    flex: 1,
  },
  text: {
    marginRight: 10,
    fontSize: 9,
  },
});

export const useStyles = () => ({
  ...styles,
  line: (borderColor: string) => ({
    ...styles.line,
    borderColor,
  }),
  text: (color: string) => ({
    ...styles.text,
    color,
  }),
});
