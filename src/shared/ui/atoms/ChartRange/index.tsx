import React from 'react';
import {Text, View} from 'react-native';
import {numberFormat} from '@shared/lib/numberFormat';
import {useStyles} from '@shared/ui/atoms/ChartRange/styles';
import {useTheme} from '@theme/hook/useTheme';

interface ChartRangeProps {
  value: string;
}

export const ChartRange: React.FC<ChartRangeProps> = ({value}) => {
  const {theme} = useTheme();
  const styles = useStyles();

  return (
    <View style={styles.range}>
      <Text style={styles.text(theme.primary)}>
        {numberFormat({number: value, prefix: '$', decimal: 2})}
      </Text>
      <View style={styles.line(theme.background)} />
    </View>
  );
};
