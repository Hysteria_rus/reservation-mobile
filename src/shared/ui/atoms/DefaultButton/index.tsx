import React, {ReactElement} from 'react';
import {Pressable, StyleProp, Text, TextStyle, ViewStyle} from 'react-native';
import {getButtonDefaultStyle} from '@shared/ui/atoms/DefaultButton/styles';
import {useTheme} from '@theme/hook/useTheme';
import {Loader} from '@shared/ui/molecules/Loader';

interface ButtonProps {
  text?: string;
  onPress?: () => void;
  buttonType?: 'primary' | 'secondary';
  children?: ReactElement;
  viewStyle?: StyleProp<ViewStyle>;
  textStyle?: StyleProp<TextStyle>;
  isDisabled?: boolean;
  height?: number | string;
  isLoading?: boolean;
}

export const DefaultButton: React.FC<ButtonProps> = ({
  text,
  buttonType = 'primary',
  onPress,
  children,
  viewStyle,
  textStyle,
  isDisabled,
  height = 'auto',
  isLoading = false,
}) => {
  const {theme} = useTheme();
  const styles = getButtonDefaultStyle({
    buttonType,
    isDisabled,
    primaryBackgroundColor: theme.primary,
    secondaryBorderColor: theme.primary,
    primaryTextColor: theme.white,
    secondaryTextColor: theme.primary,
    height,
  });
  return (
    <Pressable
      disabled={isDisabled}
      onPress={onPress}
      style={[styles.view, viewStyle]}>
      {isLoading ? (
        <Loader />
      ) : text ? (
        <Text style={[styles.text, textStyle]}>{text}</Text>
      ) : (
        children
      )}
    </Pressable>
  );
};
