import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  tab: {
    height: 28,
    flex: 1,
  },
  text: {
    fontSize: 12,
    fontWeight: '400',
  },
});

export const useStyles = () => ({
  tab: (backgroundColor: string) => ({
    ...styles.tab,
    backgroundColor,
  }),
  text: (color: string) => ({
    ...styles.text,
    color,
  }),
});
