import React from 'react';
import {DefaultButton} from '@shared/ui/atoms/DefaultButton';
import {useStyles} from '@shared/ui/atoms/Tab/styles';
import {useTheme} from '@theme/hook/useTheme';

export interface TabProps {
  label: string;
  isActive: boolean;
  onPress: () => void;
}

export const Tab: React.FC<TabProps> = ({label, isActive, onPress}) => {
  const {theme} = useTheme();

  const styles = useStyles();

  return (
    <DefaultButton
      viewStyle={styles.tab(
        isActive ? theme.primary : theme.infoBoardBackground,
      )}
      textStyle={styles.text(isActive ? theme.white : theme.black)}
      onPress={onPress}
      text={label}
    />
  );
};
