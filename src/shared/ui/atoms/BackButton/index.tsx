import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {Icon} from '@shared/ui/atoms/Icon';
import {useTheme} from '@theme/hook/useTheme';
import {Pressable, StyleProp, ViewStyle} from 'react-native';

interface BackButtonProps {
  style?: StyleProp<ViewStyle>;
}

export const BackButton: React.FC<BackButtonProps> = ({style}) => {
  const {goBack} = useNavigation();
  const {theme} = useTheme();

  return (
    <Pressable onPress={goBack} style={style}>
      <Icon name="backArrow" color={theme.primary} />
    </Pressable>
  );
};
