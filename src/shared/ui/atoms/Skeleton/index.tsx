import React, {ReactElement} from 'react';
import {View, StyleProp, ViewStyle} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import {useStyles} from './styles';

interface SkeletonProps {
  isLoading?: boolean;
  width?: string | number;
  height?: string | number;
  children: ReactElement;
  style?: StyleProp<ViewStyle>;
}

export const Skeleton: React.FC<SkeletonProps> = ({
  children,
  isLoading = false,
  width = '100%',
  height = '100%',
  style,
}) => {
  const styles = useStyles();
  return (
    <>
      {isLoading ? (
        <SkeletonPlaceholder>
          <View style={[styles.skeleton(width, height), style]} />
        </SkeletonPlaceholder>
      ) : (
        children
      )}
    </>
  );
};
