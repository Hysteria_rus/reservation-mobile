import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  skeleton: {
    borderRadius: 12,
    marginVertical: 5,
  },
});

export const useStyles = () => ({
  skeleton: (width: string | number, height: string | number) => ({
    ...styles.skeleton,
    width,
    height,
  }),
});
