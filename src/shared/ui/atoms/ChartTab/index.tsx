import React from 'react';
import {Pressable, Text} from 'react-native';
import {useTranslation} from 'react-i18next';
import {useStyles} from '@shared/ui/atoms/ChartTab/styles';
import {useTheme} from '@theme/hook/useTheme';

export interface ChartTabProps {
  label: string;
  value: string;
  isActive: boolean;
  onPress: (value: string) => void;
}

export const ChartTab: React.FC<ChartTabProps> = ({
  label,
  isActive,
  onPress,
  value,
}) => {
  const {t} = useTranslation();
  const styles = useStyles();
  const {theme} = useTheme();

  return (
    <Pressable
      onPress={() => onPress(value)}
      style={styles.tab(isActive ? theme.background : theme.white)}>
      <Text style={styles.text(theme.black)}>{t(label)}</Text>
    </Pressable>
  );
};
