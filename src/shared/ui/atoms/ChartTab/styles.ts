import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  tab: {
    paddingVertical: 4,
    paddingHorizontal: 12,
    borderRadius: 4,
    minWidth: 48,
    alignItems: 'center',
  },
  text: {
    fontSize: 12,
  },
});

export const useStyles = () => ({
  ...styles,
  tab: (backgroundColor: string) => ({
    ...styles.tab,
    backgroundColor,
  }),
  text: (color: string) => ({
    ...styles.text,
    color,
  }),
});
