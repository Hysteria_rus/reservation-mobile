import {TextStyle} from 'react-native';

export const useStyles = () => ({
  text: ({
    color,
    fontWeight = '400',
    fontSize = 14,
    lineHeight,
  }: TextStyle) => ({
    fontSize,
    fontWeight,
    lineHeight,
    color,
  }),
});
