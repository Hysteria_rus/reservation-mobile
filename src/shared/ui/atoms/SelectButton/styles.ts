import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  button: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  icon: {
    marginLeft: 10,
    fontWeight: '900',
  },
});

export const useStyles = () => ({
  ...styles,
  button: ({
    variation,
    backgroundColor,
  }: {
    variation: 'primary' | 'secondary';
    backgroundColor: string;
  }) => {
    if (variation === 'primary') {
      return {
        ...styles.button,
        padding: 8,
        borderRadius: 32,
        backgroundColor,
      };
    }
    return {
      ...styles.button,
    };
  },
  icon: (color: string) => ({
    ...styles.icon,
    color,
  }),
});
