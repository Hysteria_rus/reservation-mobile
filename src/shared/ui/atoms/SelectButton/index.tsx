import React, {ReactElement} from 'react';
import {Pressable, StyleProp, ViewStyle} from 'react-native';
import {Icon} from '@shared/ui/atoms/Icon';
import {useStyles} from '@shared/ui/atoms/SelectButton/styles';
import {useTheme} from '@theme/hook/useTheme';
import {IconName} from '@shared/config/iconsList';

interface SelectButtonProps {
  children: ReactElement;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  variation?: 'primary' | 'secondary';
  arrowColor?: string;
  backgroundColor?: string;
  icon?: IconName;
}

export const SelectButton: React.FC<SelectButtonProps> = ({
  onPress,
  children,
  style,
  arrowColor,
  variation = 'primary',
  backgroundColor,
  icon = 'dropDownArrow',
}) => {
  const styles = useStyles();
  const {theme} = useTheme();

  return (
    <Pressable
      style={[
        styles.button({
          variation,
          backgroundColor: backgroundColor || theme.white,
        }),
        style,
      ]}
      onPress={onPress}>
      {children}
      <Icon
        width={18}
        height={18}
        style={styles.icon(arrowColor || theme.primary)}
        name={icon}
      />
    </Pressable>
  );
};
