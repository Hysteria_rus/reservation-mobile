import {StyleSheet, ViewStyle} from 'react-native';

const styles = StyleSheet.create({
  tooltipContent: {
    padding: 12,
    borderRadius: 12,
    borderWidth: 1,
    width: '100%',
    height: '100%',
  },
});

export const useStyles = () => ({
  tooltipContent: ({
    backgroundColor,
    borderColor,
  }: Pick<ViewStyle, 'backgroundColor' | 'borderColor'>) => ({
    ...styles.tooltipContent,
    backgroundColor,
    borderColor,
  }),
});
