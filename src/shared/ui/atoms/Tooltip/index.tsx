import React, {ReactElement} from 'react';
import TooltipLib, {TooltipProps} from 'react-native-walkthrough-tooltip';
import {Pressable, View} from 'react-native';
import {useStyles} from '@shared/ui/atoms/Tooltip/styles';
import {useTheme} from '@theme/hook/useTheme';

interface CustomTooltipProps {
  children: ReactElement | ReactElement[];
  content: ReactElement | ReactElement[];
  isVisible: boolean;
  onClose: () => void;
  tooltipProps?: Omit<TooltipProps, 'isVisible' | 'onClose' | 'content'>;
}

export const Tooltip: React.FC<CustomTooltipProps> = ({
  children,
  content,
  onClose,
  isVisible,
  tooltipProps,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();

  return (
    <TooltipLib
      isVisible={isVisible}
      onClose={onClose}
      backgroundColor="transparent"
      contentStyle={styles.tooltipContent({
        borderColor: theme.inputBorderColor,
        backgroundColor: theme.infoBoardBackground,
      })}
      disableShadow
      arrowSize={{width: 0, height: 0}}
      content={<View>{content}</View>}
      {...tooltipProps}>
      <Pressable>{children}</Pressable>
    </TooltipLib>
  );
};
