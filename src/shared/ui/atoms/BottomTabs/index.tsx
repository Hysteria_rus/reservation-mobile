import {useNavigation} from '@react-navigation/native';
import {IconName} from '@shared/config/iconsList';
import {useTheme} from '@theme/hook/useTheme';
import React from 'react';
import {View, Pressable, Text} from 'react-native';
import {Icon} from '../Icon';
import {BottomTabEnum} from './config';
import {useStyles} from './styles';

interface BottomTabItemProps {
  routeNames: string[];
  focused: number;
}

export const BottomTabs: React.FC<BottomTabItemProps> = ({
  focused,
  routeNames,
}) => {
  const navigation = useNavigation();
  const {theme} = useTheme();

  const styles = useStyles();

  const getTabName = (route: string) => {
    return BottomTabEnum[route as keyof typeof BottomTabEnum].toLowerCase();
  };

  const getColor = (index: number) => {
    return focused === index ? theme.primary : theme.inputBorderColor;
  };

  return (
    <View style={styles.container}>
      {routeNames.map((item, index) => (
        <>
          {item === 'ActionScreen' ? (
            <Pressable
              style={styles.tab(theme.primary, 22)}
              key={index}
              onPress={() => {}}>
              <Icon
                style={styles.icon('white')}
                name={getTabName(item).toLowerCase() as IconName}
              />
            </Pressable>
          ) : (
            <Pressable
              style={styles.tab('white', 0)}
              key={index}
              onPress={() => navigation.navigate(item)}>
              <Icon
                style={styles.icon(getColor(index))}
                name={getTabName(item).toLowerCase() as IconName}
              />
              <Text style={styles.text(getColor(index))}>
                {getTabName(item)}
              </Text>
            </Pressable>
          )}
        </>
      ))}
    </View>
  );
};
