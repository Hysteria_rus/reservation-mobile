import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 32,
    paddingTop: 14,
    paddingBottom: 30,
    height: 80,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: 'white',
    shadowOffset: {
      width: 0,
      height: -16,
    },
    shadowOpacity: 0.14,
    shadowRadius: 16,
    shadowColor: 'rgb(136, 165, 191)',
    elevation: 10,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
  },
  icon: {
    display: 'flex',
  },
  tab: {
    height: 44,
    width: 44,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontSize: 12,
    fontWeight: '500',
    lineHeight: 12,
    marginTop: 8,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    icon: (color: string) => ({
      ...styles.icon,
      color,
    }),
    text: (color: string) => ({
      ...styles.text,
      color,
    }),
    tab: (backgroundColor: string, borderRadius: number) => ({
      ...styles.tab,
      backgroundColor,
      borderRadius,
    }),
  };
};
