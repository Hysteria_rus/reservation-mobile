import React from 'react';
import {StyleProp, Text, View, ViewStyle} from 'react-native';
import {Icon} from '@shared/ui/atoms/Icon';
import {useStyles} from '@shared/ui/atoms/CurrencyRate/styles';
import {useTheme} from '@theme/hook/useTheme';
import BigNumber from 'bignumber.js';
import {numberFormat} from '@shared/lib/numberFormat';
import {replaceComma} from '@shared/lib/replaceComma';
import {IconName} from '@shared/config/iconsList';

interface CurrencyRate {
  rate: string;
  rateChange: string;
  style?: StyleProp<ViewStyle>;
  iconName: IconName;
}

export const CurrencyRate: React.FC<CurrencyRate> = ({
  rate,
  rateChange,
  style,
  iconName,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();

  return (
    <View style={[styles.row, style]}>
      <Icon name={iconName} width={40} height={40} />
      <Text style={styles.rate(theme.black)}>
        {numberFormat({number: new BigNumber(replaceComma(rate)), prefix: '$'})}
      </Text>
      <Text
        style={styles.changeRate(
          new BigNumber(rateChange).isGreaterThan(0)
            ? theme.success
            : theme.rejected,
        )}>
        {rateChange}%
      </Text>
    </View>
  );
};
