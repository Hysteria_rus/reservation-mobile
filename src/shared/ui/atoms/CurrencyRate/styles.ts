import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: 40,
    height: 40,
  },
  rate: {
    fontWeight: '900',
    fontSize: 21,
    marginHorizontal: 8,
  },
});

export const useStyles = () => ({
  ...styles,
  rate: (color: string) => ({
    ...styles.rate,
    color,
  }),
  changeRate: (color: string) => ({
    color,
  }),
});
