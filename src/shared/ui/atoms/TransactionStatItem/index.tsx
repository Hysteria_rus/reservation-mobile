import React from 'react';
import {View} from 'react-native';
import {Icon} from '@shared/ui/atoms/Icon';
import {Text} from '@shared/ui/atoms/Text';
import {numberFormat} from '@shared/lib/numberFormat';
import {IconName} from '@shared/config/iconsList';
import {useTheme} from '@theme/hook/useTheme';
import {styles} from '@shared/ui/atoms/TransactionStatItem/styles';

interface TransactionStatItemProps {
  iconName: IconName;
  type: string;
  amount: string;
}

export const TransactionStatItem: React.FC<TransactionStatItemProps> = ({
  type,
  amount,
  iconName,
}) => {
  const {theme} = useTheme();

  return (
    <View style={styles.wrapper}>
      <View style={styles.row}>
        <Icon
          style={styles.icon}
          width={12}
          height={12}
          color={theme.description}
          name={iconName}
        />
        <Text color={theme.description} fontSize={11} fontWeight="600">
          {type}
        </Text>
      </View>
      <Text fontSize={11} fontWeight="400">
        {numberFormat({number: amount, prefix: '$'})}
      </Text>
    </View>
  );
};
