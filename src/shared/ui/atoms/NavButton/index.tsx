import React from 'react';
import {Pressable} from 'react-native';
import {Icon} from '@shared/ui/atoms/Icon';
import {IconName} from '@shared/config/iconsList';
import {useStyles} from '@shared/ui/atoms/NavButton/styles';
import {useTheme} from '@theme/hook/useTheme';
import {Text} from '@shared/ui/atoms/Text';

interface LinkButtonProps {
  iconName: IconName;
  text: string;
  onPress: () => void;
}
export const NavButton: React.FC<LinkButtonProps> = ({
  text,
  iconName,
  onPress,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();

  return (
    <Pressable style={styles.container(theme.white)} onPress={onPress}>
      <Icon
        width={18}
        height={18}
        style={styles.icon(theme.primary)}
        name={iconName}
      />
      <Text fontSize={12} lineHeight={14}>{text}</Text>
    </Pressable>
  );
};
