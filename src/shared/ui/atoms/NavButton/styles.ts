import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    height: 72,
    borderRadius: 12,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    marginHorizontal: 4,
  },
  icon: {
    marginBottom: 10,
  },
});

export const useStyles = () => ({
  ...styles,
  container: (backgroundColor: string) => ({
    ...styles.container,
    backgroundColor,
  }),
  icon: (color: string) => ({
    ...styles.icon,
    color,
  }),
});
