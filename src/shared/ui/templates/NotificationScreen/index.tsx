import React, {ReactElement} from 'react';
import {PageTemplate} from '@shared/ui/templates/Page';
import {Icon} from '@shared/ui/atoms/Icon';
import {DefaultButton} from '@shared/ui/atoms/DefaultButton';
import {useTranslation} from 'react-i18next';
import {IconName} from '@shared/config/iconsList';
import {Text} from '@shared/ui/atoms/Text';
import {View} from 'react-native';
import {styles} from '@shared/ui/templates/NotificationScreen/styles';

interface NotificationScreen {
  btnText?: string;
  iconName: IconName;
  text?: string;
  children?: ReactElement | ReactElement[];
  onPress: () => void;
}

export const NotificationScreen: React.FC<NotificationScreen> = ({
  btnText,
  iconName,
  text,
  children,
  onPress,
}) => {
  const {t} = useTranslation();

  return (
    <PageTemplate scrollProps={{scrollEnabled: false}} style={styles.component}>
      <View style={styles.content}>
        <Icon width={160} height={160} name={iconName} />
        <View style={styles.textWrapper}>
          {text && (
            <Text style={styles.text} fontWeight="400">
              {text}
            </Text>
          )}
          {children}
        </View>
      </View>
      <DefaultButton
        viewStyle={styles.button}
        text={btnText || t('common.done')}
        onPress={onPress}
      />
    </PageTemplate>
  );
};
