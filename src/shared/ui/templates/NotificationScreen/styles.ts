import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  content: {
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textWrapper: {
    marginTop: 28,
  },
  text: {
    textAlign: 'center',
  },
  button: {
    height: 56,
    marginTop: 'auto',
  },
});
