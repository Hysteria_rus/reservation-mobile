import React from 'react';
import {View} from 'react-native';
import {Text} from '@shared/ui/atoms/Text';
import {useTranslation} from 'react-i18next';
import {useTheme} from '@theme/hook/useTheme';
import {styles} from '@shared/ui/templates/EmptyTransactionList/styles';

export const EmptyTransactionList: React.FC = () => {
  const {t} = useTranslation();
  const {theme} = useTheme();

  return (
    <View style={styles.container}>
      <Text color={theme.primary} fontSize={20} fontWeight="700">
        {t('transactions.emptyTransaction')}
      </Text>
    </View>
  );
};
