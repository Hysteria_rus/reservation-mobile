import React from 'react';
import {Text, View} from 'react-native';
import {BoardWithContent} from '@shared/ui/atoms/BoardWithContent';
import {SCREEN_WIDTH} from '@shared/constants/screenSize';
import {useTheme} from '@theme/hook/useTheme';
import {Icon} from '@shared/ui/atoms/Icon';
import {useStyles} from '@shared/ui/templates/CompleteTransaction/styles';
import {DefaultButton} from '@shared/ui/atoms/DefaultButton';
import {useTranslation} from 'react-i18next';
import {IconName} from '@shared/config/iconsList';

interface CompleteTransactionProps {
  fromName: string;
  fromTicker: string;
  toName: string;
  toTicker?: string;
  amount?: string;
  value: string;
  onPress: () => void;
}

export const CompleteTransaction: React.FC<CompleteTransactionProps> = ({
  fromName,
  fromTicker,
  toName,
  toTicker,
  amount,
  value,
  onPress,
}) => {
  const {theme} = useTheme();
  const styles = useStyles();
  const {t} = useTranslation();
  const toNameReplace = toName.replace(toName.substr(3, 15), '...');
  const toNameString = toName.length > 10 ? toNameReplace : toName;
  return (
    <View style={styles.wrapper}>
      <Icon name="complete" width={56} height={56} />
      <View style={styles.fromToWrapper}>
        <BoardWithContent
          wrapperStyle={styles.from}
          borderRadius={12}
          height={64}
          width={SCREEN_WIDTH / 2}
          backgroundColor={theme.grey}>
          <View style={styles.boardContentWrapper}>
            <Icon name={fromTicker as IconName} width={32} height={32} />
            <Text style={styles.text(theme.white)}>
              {fromName} ({fromTicker})
            </Text>
          </View>
        </BoardWithContent>
        <Icon
          name="deformatedArrow"
          width={16}
          height={16}
          color={theme.primary}
          style={styles.deformatedArrow}
        />
        <BoardWithContent
          wrapperStyle={styles.to}
          borderRadius={12}
          height={64}
          width={SCREEN_WIDTH / 2}
          backgroundColor={theme.background}>
          <Text style={styles.text(theme.black)}>
            {toNameString} {toTicker && `(${toTicker})`}
          </Text>
        </BoardWithContent>
      </View>
      <View style={styles.valueWrapper}>
        <Text>
          {amount} {fromTicker}
        </Text>
        <Icon
          name="slimRightArrow"
          width={24}
          height={20}
          color={theme.black}
          style={styles.arrowIcon}
        />
        <Text>
          {value} {fromTicker}
        </Text>
      </View>
      <DefaultButton
        text={t('common.done')}
        buttonType="primary"
        viewStyle={styles.button}
        onPress={onPress}
      />
    </View>
  );
};
