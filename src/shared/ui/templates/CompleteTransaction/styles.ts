import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    marginTop: 150,
    flex: 1,
  },
  fromToWrapper: {
    flexDirection: 'row',
    marginTop: 24,
  },
  from: {
    position: 'absolute',
    right: -38,
    paddingHorizontal: 16,
    zIndex: 2,
  },
  to: {
    marginTop: 45,
    left: -38,
    zIndex: 3,
    position: 'absolute',
    paddingHorizontal: 16,
  },
  boardContentWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  deformatedArrow: {
    position: 'absolute',
    left: 50,
    top: 20,
    zIndex: 4,
  },
  text: {
    fontWeight: '600',
    fontSize: 14,
    lineHeight: 18,
  },
  valueWrapper: {
    flexDirection: 'row',
    marginTop: 120,
  },
  arrowIcon: {
    marginHorizontal: 16,
  },
  button: {
    width: '100%',
    height: 56,
    marginTop: 'auto',
  },
});

export const useStyles = () => {
  return {
    ...styles,
    text: (color: string) => ({
      ...styles.text,
      color,
    }),
  };
};
