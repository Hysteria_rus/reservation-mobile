import React, {ReactElement} from 'react';
import {KeyboardTypeOptions, StyleProp, View, ViewStyle} from 'react-native';
import {Input} from '@shared/ui/molecules/Input';
import {DefaultButton} from '@shared/ui/atoms/DefaultButton';
import {RenderInput} from '@shared/config/types/input';
import {useStyles} from '@shared/ui/templates/Form/styles';
import {Text} from '@shared/ui/atoms/Text';

interface FormTemplateProps {
  btnText?: string;
  inputs: RenderInput[];
  children?: ReactElement;
  style?: StyleProp<ViewStyle>;
  title?: string;
  onPress?: () => void;
  keyboardType?: KeyboardTypeOptions;
  isLoading?: boolean;
  disabled?: boolean;
}

export const FormTemplate: React.FC<FormTemplateProps> = ({
  btnText = 'Next',
  inputs,
  children,
  style,
  title,
  onPress,
  keyboardType,
  isLoading = false,
  disabled = false,
}) => {
  const styles = useStyles();

  return (
    <View style={[styles.template, style]}>
      {title && (
        <Text fontSize={14} lineHeight={18} style={styles.title}>
          {title}
        </Text>
      )}
      <View>
        {inputs.map((input, index) => (
          <Input
            value={input.value}
            upContent={input?.upContent}
            key={index}
            label={input?.label}
            onChange={value => input.onChangeText(value)}
            inputProps={{
              ...input.inputProps,
              placeholder: input?.placeholder,
            }}
            errorText={input?.errorText}
            style={[styles.input, input.style]}
            rightContent={input.rightContent}
            keyboardType={keyboardType}
          />
        ))}
      </View>
      {children}
      <DefaultButton
        isDisabled={isLoading || disabled}
        isLoading={isLoading}
        viewStyle={styles.button}
        height={56}
        buttonType="primary"
        text={btnText}
        onPress={onPress}
      />
    </View>
  );
};
