import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  template: {
    flex: 1,
  },
  title: {
    marginBottom: 40,
  },
  input: {
    marginBottom: 24,
  },

  button: {
    marginTop: 'auto',
  },
});

export const useStyles = () => ({
  ...styles,
});
