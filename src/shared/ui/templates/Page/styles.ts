import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  page: {
    paddingTop: 16,
    paddingHorizontal: 16,
    paddingBottom: 16,
    flex: 1,
  },
  scrollView: {
    minHeight: '100%',
  },
  safeArea: {
    flex: 1,
  },
});

export const useStyles = () => ({
  ...styles,
  page: (backgroundColor: string) => ({
    ...styles.page,
    backgroundColor,
  }),

  safeArea: (backgroundColor: string) => ({
    ...styles.safeArea,
    backgroundColor,
  }),
});
