import React, {ReactElement, useCallback, useRef} from 'react';
import {ScrollViewProps, StyleProp, View, ViewStyle} from 'react-native';
import {useStyles} from '@shared/ui/templates/Page/styles';
import {useTheme} from '@theme/hook/useTheme';
import {ScrollView} from 'react-native-gesture-handler';
import {useFocusEffect} from '@react-navigation/native';
import {scrollTop} from '@shared/lib/scrollTo';
import {setSafeAreaColor} from '@shared/model/currentSafeAreaColor';

interface PageTemplateProps {
  backgroundColor?: string;
  children: ReactElement | ReactElement[];
  scrollProps?: ScrollViewProps;
  style?: StyleProp<ViewStyle>;
}

export const PageTemplate: React.FC<PageTemplateProps> = ({
  children,
  backgroundColor,
  scrollProps,
  style,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();
  const scrollViewRef = useRef(null);
  useFocusEffect(
    useCallback(() => {
      setSafeAreaColor(backgroundColor || theme.white);
      return () => {
        scrollTop(scrollViewRef);
      };
    }, []),
  );
  return (
    <View style={styles.safeArea(backgroundColor || theme.white)}>
      <ScrollView
        ref={scrollViewRef}
        contentContainerStyle={styles.scrollView}
        {...scrollProps}
        showsVerticalScrollIndicator={false}>
        <View style={[styles.page(backgroundColor || theme.white), style]}>
          {children}
        </View>
      </ScrollView>
    </View>
  );
};
