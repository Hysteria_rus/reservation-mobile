import React from 'react';
import ReactModal from 'react-native-modal';
import {modalStyles} from '@shared/ui/templates/Modal/styles';
import {useStore} from 'effector-react';
import {$openedModal, closeAllModals} from '@shared/lib/modalFactory';

export const Modal: React.FC = () => {
  const {component: Component} = useStore($openedModal);
  return (
    <ReactModal
      style={modalStyles.childrenWrapper}
      onBackdropPress={closeAllModals}
      isVisible={Boolean(Component)}
      backdropOpacity={0.9}
      backdropTransitionOutTiming={0}
      propagateSwipe={false}>
      {Component && <Component />}
    </ReactModal>
  );
};
