import {StyleSheet} from 'react-native';
import {SCREEN_WIDTH} from '@shared/constants/screenSize';

export const modalStyles = StyleSheet.create({
  childrenWrapper: {
    flex: 1,
    margin: 0,
    padding: 0,
  },
});
