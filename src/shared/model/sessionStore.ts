import {MMKV} from '@shared/lib/initMMKV';
import {
  createEvent,
  createEffect,
  restore,
  createStore,
  combine,
  forward,
} from 'effector';
import jwtDecode from 'jwt-decode';

type SessionType = {
  exp: number;
};
const setNow = createEvent();

const $now = createStore(new Date().getTime() / 1000).on(
  setNow,
  prev => prev + 5,
);
const getSession = (): SessionType => {
  const token = MMKV.getString('@accessToken');
  if (token) {
    return jwtDecode(token) as SessionType;
  } else {
    return {exp: 0};
  }
};

const setSession = createEvent();
const setSessionFx = createEffect(() => {
  return getSession();
});
const $session = restore(setSessionFx, getSession());
forward({
  from: setSession,
  to: setSessionFx,
});

const $isSessionLost = combine([$session, $now], ([session, now]) => {
  return (
    parseFloat(session?.exp.toString() || '0') <= parseFloat(now.toString())
  );
});

const $hasSession = $session.map(_ => _.exp !== 0);

export const sessionStore = {
  setSession,
  $isSessionLost,
  $hasSession,
};
