import {createEvent, restore} from 'effector';
import {themes} from '../../theme/themes';

const setSafeAreaColor = createEvent<string>();

const setIsBottomBar = createEvent<boolean>();

const reset = createEvent();

const $currentSafeAreaColor = restore(
  setSafeAreaColor,
  themes.primary.white,
).reset();

const $isBottomBar = restore(setIsBottomBar, false).reset(reset);

export {
  $currentSafeAreaColor,
  setSafeAreaColor,
  $isBottomBar,
  setIsBottomBar,
  reset,
};
