import {useTheme} from '@theme/hook/useTheme';
import React from 'react';
import {View} from 'react-native';
import {useStyles} from './styles';

export const HomeScreen: React.FC = () => {
  const styles = useStyles();
  const {theme} = useTheme();
  return <View style={styles.container(theme.background)}></View>;
};
