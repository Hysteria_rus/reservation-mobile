import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  text: {
    marginBottom: 24,
  },
});

export const useStyles = () => ({
  ...styles,
  highlighted: (color: string) => ({
    color,
  }),
});
