import React, {useEffect} from 'react';
import {TwoFactorForm} from '@features/TwoFactor';
import {PageTemplate} from '@shared/ui/templates/Page';
import {useTheme} from '@theme/hook/useTheme';

export const TwoFactor: React.FC = () => {
  const {theme} = useTheme();

  return (
    <PageTemplate backgroundColor={theme.background}>
      <TwoFactorForm />
    </PageTemplate>
  );
};
