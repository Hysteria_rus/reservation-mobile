import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    flex: 1,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    container: (backgroundColor: string) => ({
      ...styles.container,
      backgroundColor,
    }),
  };
};
