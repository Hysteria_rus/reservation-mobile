import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  forgotPassword: {
    textAlign: 'center',
  },
});

export const useStyles = () => ({
  ...styles,
  forgotPassword: (color: string) => ({
    ...styles.forgotPassword,
    color,
  }),
});
