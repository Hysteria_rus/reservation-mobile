import React, {useCallback, useEffect} from 'react';
import {LogInForm} from '@features/LogIn';
import {Pressable} from 'react-native';
import {useTheme} from '@theme/hook/useTheme';
import {useStyles} from './styles';
import {PageTemplate} from '@shared/ui/templates/Page';
import {useTranslation} from 'react-i18next';

export const LogIn: React.FC = () => {
  const {theme} = useTheme();
  const styles = useStyles();

  const {t} = useTranslation();

  return (
    <PageTemplate>
      <LogInForm />
    </PageTemplate>
  );
};
