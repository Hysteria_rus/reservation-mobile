import {DefaultButton} from '@shared/ui/atoms/DefaultButton';
import {Icon} from '@shared/ui/atoms/Icon';
import {useTheme} from '@theme/hook/useTheme';
import React, {useCallback} from 'react';
import {View} from 'react-native';
import {useStyles} from './styles';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import {AuthStackParams} from '@shared/config/types/navigation/AuthStackParams';
import {StackNavigationProp} from '@react-navigation/stack';
import {useTranslation} from 'react-i18next';
import {normalButtonStyle} from '@shared/styles/button';
import {setSafeAreaColor} from '@shared/model/currentSafeAreaColor';

export const WelcomeScreen: React.FC = () => {
  const styles = useStyles();
  const {theme} = useTheme();
  const {navigate} =
    useNavigation<StackNavigationProp<AuthStackParams, 'LogIn'>>();
  const {t} = useTranslation();
  const goToLogin = () => {
    navigate('LogIn');
  };
  useFocusEffect(
    useCallback(() => {
      setSafeAreaColor(theme.background);
    }, []),
  );
  return (
    <View style={styles.container(theme.background)}>
      <Icon
        style={styles.icon}
        color={theme.primary}
        name="welcome"
        height={400}
        width={400}
      />
      <View style={styles.buttonContainer}>
        <DefaultButton
          buttonType="primary"
          text={t('welcome.logIn')}
          onPress={goToLogin}
          viewStyle={normalButtonStyle.height}
        />
      </View>
    </View>
  );
};
