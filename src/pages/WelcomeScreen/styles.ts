import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    alignItems: 'flex-start',
    flex: 1,
  },
  buttonContainer: {
    marginTop: 'auto',
    width: '100%',
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  button: {
    marginBottom: 8,
    height: 56,
  },
  icon: {
    marginTop: 70,
  },
});

export const useStyles = () => {
  return {
    ...styles,
    container: (backgroundColor: string) => ({
      ...styles.container,
      backgroundColor,
    }),
  };
};
