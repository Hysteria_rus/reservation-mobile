import React from 'react';
import {Pressable, Text} from 'react-native';
import {useStyles} from './styles';
import {useTheme} from '@theme/hook/useTheme';
import {TfaType} from '@shared/api/apollo/__generated__';
import {useTranslation} from 'react-i18next';

interface ResendCodeButtonProps {
  type: TfaType;
}

export const ResendCodeButton: React.FC<ResendCodeButtonProps> = ({type}) => {
  const {t} = useTranslation();
  const styles = useStyles();
  const {theme} = useTheme();
  return (
    <Pressable onPress={() => {}}>
      <Text style={styles.buttonText(theme.primary)}>
        {t('common.sendCode')}
      </Text>
    </Pressable>
  );
};
