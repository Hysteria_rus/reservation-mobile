import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  buttonText: {
    fontSize: 14,
    fontWeight: '600',
  },
  highlightedText: {
    marginLeft: 4,
  },
  container: {
    marginBottom: 16,
    flexDirection: 'row',
  },
});

export const useStyles = () => ({
  ...styles,
  buttonText: (color: string) => ({
    ...styles.buttonText,
    color,
  }),
});
