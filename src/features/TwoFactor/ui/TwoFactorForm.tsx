import React, {useEffect} from 'react';
import {FormTemplate} from '@shared/ui/templates/Form';
import {useRenderInputs} from '@features/TwoFactor/lib/useRenderInputs';
import {twoFactorForm} from '../model';
import {useForm} from 'effector-forms';

export const TwoFactorForm: React.FC = () => {
  const {submit} = useForm(twoFactorForm);

  const inputs = useRenderInputs();

  return (
    <FormTemplate onPress={submit} inputs={inputs} keyboardType="phone-pad" />
  );
};
