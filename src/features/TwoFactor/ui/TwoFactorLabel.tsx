import React from 'react';
import {View} from 'react-native';
import {useStyles} from './styles';
import {useTheme} from '@theme/hook/useTheme';
import {Text} from '@shared/ui/atoms/Text';

interface TwoFactorLabelProps {
  text: string;
  highlightedText?: string;
}

export const TwoFactorLabel: React.FC<TwoFactorLabelProps> = ({
  text,
  highlightedText,
}) => {
  const styles = useStyles();
  const {theme} = useTheme();
  return (
    <View style={styles.container}>
      <Text fontSize={14} lineHeight={18}>
        {text}
      </Text>
      <Text
        fontSize={14}
        lineHeight={18}
        color={theme.primary}
        style={styles.highlightedText}>
        {highlightedText}
      </Text>
    </View>
  );
};
