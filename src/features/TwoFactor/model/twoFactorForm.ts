import {createForm} from 'effector-forms';
import * as yup from 'yup';
import {commonInputRules} from '@shared/config/commonInputRules';
import {createRule} from '@shared/lib/createRule';

const rules = {
  length: createRule<string>({
    name: 'amountMax',
    schema: yup.string().max(6).min(6),
  }),
};

const twoFactorForm = createForm({
  fields: {
    SMS: {
      init: '',
      rules: [commonInputRules.required, rules.length],
    },
  },
  validateOn: [],
});

export {twoFactorForm};
