import {RenderInput} from '@shared/config/types/input';
import {useForm} from 'effector-forms';
import {useTranslation} from 'react-i18next';
import {twoFactorForm} from '../model';
import i18next from 'i18next';

export const useRenderInputs = (): RenderInput[] => {
  const {fields, errorText} = useForm(twoFactorForm);
  const {t} = useTranslation();

  return [
    {
      label: t('login.SMSCode'),
      placeholder: t('login.sms'),
      onChangeText: (value: string) => fields.SMS.onChange(value),
      value: fields.SMS.value,
      errorText: i18next.t(errorText('SMS')),
    },
  ];
};
