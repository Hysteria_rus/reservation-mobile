import {useForm} from 'effector-forms';
import {loginForm} from '@features/LogIn/model';
import {RenderInput} from '@shared/config/types/input';
import {useTranslation} from 'react-i18next';
import i18next from 'i18next';

export const useRenderInputs = (): RenderInput[] => {
  const {fields, errorText} = useForm(loginForm);
  const {t} = useTranslation();

  return [
    {
      label: t('common.phone'),
      placeholder: t('common.phone'),
      onChangeText: (value: string) => fields.phone.onChange(value),
      value: fields.phone.value,
      errorText: i18next.t(errorText('phone')),
    },
  ];
};
