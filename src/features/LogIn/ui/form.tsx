import React, {ReactElement} from 'react';
import {FormTemplate} from '@shared/ui/templates/Form';
import {useRenderInputs} from '@features/LogIn/lib/useRenderInputs';
import {useTranslation} from 'react-i18next';
import {loginForm} from '../model';
import {useStore} from 'effector-react';
import {useForm} from 'effector-forms';
import {navigate} from '@shared/lib/rootNavigation';

interface LoginFormProps {
  children?: ReactElement;
}

export const LogInForm: React.FC<LoginFormProps> = ({children}) => {
  const inputs = useRenderInputs();
  const {t} = useTranslation();
  const {submit} = useForm(loginForm);
  return (
    <FormTemplate
      onPress={() => {
        navigate('TwoFactor');
        submit();
      }}
      inputs={inputs}
      btnText={t('login.login')}>
      {children}
    </FormTemplate>
  );
};
