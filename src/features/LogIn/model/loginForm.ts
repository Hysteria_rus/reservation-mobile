import {createForm} from 'effector-forms';
import {commonInputRules} from '@shared/config/commonInputRules';

const loginForm = createForm({
  fields: {
    phone: {
      init: '',
      rules: [commonInputRules.required],
    },
  },
  validateOn: ['submit'],
});

export {loginForm};
