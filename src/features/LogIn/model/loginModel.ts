import {sample} from 'effector';
import {loginForm} from '@features/LogIn/model';
import {MMKV} from '@shared/lib/initMMKV';

sample({
  clock: loginForm.submit,
  fn: () => {
    MMKV.setString('@accessToken', 'loggedIn');
    console.log(1);
  },
});
