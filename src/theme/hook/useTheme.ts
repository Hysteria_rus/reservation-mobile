import {useContext} from 'react';

import {themes} from '../themes';
import {ThemeContext, ThemesList} from '../themeProvider';

export function useTheme() {
  const {theme, changeTheme} = useContext(ThemeContext);

  const toggleTheme = () => {
    changeTheme(
      theme === ThemesList.primary ? ThemesList.dark : ThemesList.primary,
    );
  };

  return {
    theme: Object.freeze(themes[theme]),
    changeTheme,
    toggleTheme,
    themeName: theme,
  };
}
